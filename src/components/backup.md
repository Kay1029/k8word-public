<div class="cbundle-wrapper" :style="{transform: 'rotateZ('+rotate+'deg)'}">
  <div class="line">
    <svg height="278" width="278">
      <!-- 120 -->
      <polyline v-if="getBottomUserInfo[6] > 1 && getBottomUserInfo[10] > 1"
        stroke-linecap="round" points="105,40 245,180" style="fill:none;stroke:#dd2c00;stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[2] > 1 && getBottomUserInfo[10] > 1"
        stroke-linecap="round" points="245,180 35,245" style="fill:none;stroke:#dd2c00;stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[6] > 1 && getBottomUserInfo[2] > 1"
        stroke-linecap="round" points="35,245 105,40" style="fill:none;stroke:#dd2c00;stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[8] > 1 && getBottomUserInfo[0] > 1"
        stroke-linecap="round" points="245,35 170,245" style="fill: none; stroke: rgb(25, 118, 210); stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[4] > 1 && getBottomUserInfo[0] > 1"
        stroke-linecap="round" points="170,245 35,100" style="fill: none; stroke: rgb(25, 118, 210); stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[8] > 1 && getBottomUserInfo[4] > 1"
        stroke-linecap="round" points="35,100 245,35" style="fill: none; stroke: rgb(25, 118, 210); stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[1] > 1 && getBottomUserInfo[9] > 1"
        stroke-linecap="round" points="245,105 100,245" style="fill:none;stroke:#ffca05;stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[1] > 1 && getBottomUserInfo[5] > 1"
        stroke-linecap="round" points="100,245 35,35" style="fill:none;stroke:#ffca05;stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[5] > 1 && getBottomUserInfo[9] > 1"
        stroke-linecap="round" points="35,35 245,105" style="fill:none;stroke:#ffca05;stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[7] > 1 && getBottomUserInfo[11] > 1"
        stroke-linecap="round" points="170,35 245,245" style="fill:none;stroke:#00c853;stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[3] > 1 && getBottomUserInfo[11] > 1"
        stroke-linecap="round" points="245,245 35,170" style="fill:none;stroke:#00c853;stroke-width:8;" />
      <polyline v-if="getBottomUserInfo[3] > 1 && getBottomUserInfo[7] > 1"
        stroke-linecap="round" points="35,170 170,35" style="fill:none;stroke:#00c853;stroke-width:8;" />
      <!-- 90 -->
      <polyline v-if="getBottomUserInfo[2] > 1 && getBottomUserInfo[5] > 1" stroke-linecap="round"
        :points="pos.a3[0]+','+pos.a3[1]+' '+pos.a6[0]+','+pos.a6[1]" style="fill:none;stroke:black;stroke-width:3" />
      <polyline v-if="getBottomUserInfo[8] > 1 && getBottomUserInfo[5] > 1" stroke-linecap="round"
        :points="pos.a9[0]+','+pos.a9[1]+' '+pos.a6[0]+','+pos.a6[1]" style="fill:none;stroke:black;stroke-width:3" />
      <polyline v-if="getBottomUserInfo[8] > 1 && getBottomUserInfo[11] > 1" stroke-linecap="round"
        :points="pos.a9[0]+','+pos.a9[1]+' '+pos.a12[0]+','+pos.a12[1]" style="fill:none;stroke:black;stroke-width:3" />
      <polyline v-if="getBottomUserInfo[2] > 1 && getBottomUserInfo[11] > 1" stroke-linecap="round"
        :points="pos.a3[0]+','+pos.a3[1]+' '+pos.a12[0]+','+pos.a12[1]" style="fill:none;stroke:black;stroke-width:3" />

      <polyline v-if="getBottomUserInfo[0] > 1 && getBottomUserInfo[3] > 1" stroke-linecap="round"
        :points="pos.a1[0]+','+pos.a1[1]+' '+pos.a4[0]+','+pos.a4[1]" style="fill:none;stroke:black;stroke-width:3" />
      <polyline v-if="getBottomUserInfo[6] > 1 && getBottomUserInfo[3] > 1" stroke-linecap="round"
        :points="pos.a7[0]+','+pos.a7[1]+' '+pos.a4[0]+','+pos.a4[1]" style="fill:none;stroke:black;stroke-width:3" />
      <polyline v-if="getBottomUserInfo[6] > 1 && getBottomUserInfo[9] > 1" stroke-linecap="round"
        :points="pos.a7[0]+','+pos.a7[1]+' '+pos.a10[0]+','+pos.a10[1]" style="fill:none;stroke:black;stroke-width:3" />
      <polyline v-if="getBottomUserInfo[0] > 1 && getBottomUserInfo[9] > 1" stroke-linecap="round"
        :points="pos.a1[0]+','+pos.a1[1]+' '+pos.a10[0]+','+pos.a10[1]" style="fill:none;stroke:black;stroke-width:3" />

      <polyline v-if="getBottomUserInfo[1] > 1 && getBottomUserInfo[4] > 1" stroke-linecap="round"
        :points="pos.a2[0]+','+pos.a2[1]+' '+pos.a5[0]+','+pos.a5[1]" style="fill:none;stroke:black;stroke-width:3" />
      <polyline v-if="getBottomUserInfo[7] > 1 && getBottomUserInfo[4] > 1" stroke-linecap="round"
        :points="pos.a8[0]+','+pos.a8[1]+' '+pos.a5[0]+','+pos.a5[1]" style="fill:none;stroke:black;stroke-width:3" />
      <polyline v-if="getBottomUserInfo[7] > 1 && getBottomUserInfo[10] > 1" stroke-linecap="round"
        :points="pos.a8[0]+','+pos.a8[1]+' '+pos.a11[0]+','+pos.a11[1]" style="fill:none;stroke:black;stroke-width:3" />
      <polyline v-if="getBottomUserInfo[1] > 1 && getBottomUserInfo[10] > 1" stroke-linecap="round"
        :points="pos.a2[0]+','+pos.a2[1]+' '+pos.a11[0]+','+pos.a11[1]" style="fill:none;stroke:black;stroke-width:3" />

      <!-- 180 -->
      <polyline v-if="getBottomUserInfo[0] > 1 && getBottomUserInfo[6] > 1" stroke-linecap="round"
        :points="pos.a1[0]+','+pos.a1[1]+' '+pos.a7[0]+','+pos.a7[1]" style="fill:none;stroke:rgba(0, 0, 0, 0.5);stroke-width:8" />
      <polyline v-if="getBottomUserInfo[1] > 1 && getBottomUserInfo[7] > 1" stroke-linecap="round"
        :points="pos.a2[0]+','+pos.a2[1]+' '+pos.a8[0]+','+pos.a8[1]" style="fill:none;stroke:rgba(0, 0, 0, 0.5);stroke-width:8" />
      <polyline v-if="getBottomUserInfo[2] > 1 && getBottomUserInfo[8] > 1" stroke-linecap="round"
        :points="pos.a3[0]+','+pos.a3[1]+' '+pos.a9[0]+','+pos.a9[1]" style="fill:none;stroke:rgba(0, 0, 0, 0.5);stroke-width:8" />
      <polyline v-if="getBottomUserInfo[3] > 1 && getBottomUserInfo[9] > 1" stroke-linecap="round"
        :points="pos.a4[0]+','+pos.a4[1]+' '+pos.a10[0]+','+pos.a10[1]" style="fill:none;stroke:rgba(0, 0, 0, 0.5);stroke-width:8" />
      <polyline v-if="getBottomUserInfo[4] > 1 && getBottomUserInfo[10] > 1" stroke-linecap="round"
        :points="pos.a5[0]+','+pos.a5[1]+' '+pos.a11[0]+','+pos.a11[1]" style="fill:none;stroke:rgba(0, 0, 0, 0.5);stroke-width:8" />
      <polyline v-if="getBottomUserInfo[5] > 1 && getBottomUserInfo[11] > 1" stroke-linecap="round"
        :points="pos.a5[0]+','+pos.a6[1]+' '+pos.a12[0]+','+pos.a12[1]" style="fill:none;stroke:rgba(0, 0, 0, 0.5);stroke-width:8" />

      <!-- 150 -->
      <polyline v-if="disflag.dis150 && getBottomUserInfo[0] > 1 && getBottomUserInfo[5] > 1" stroke-linecap="round"
        :points="pos.a1[0]+','+pos.a1[1]+' '+pos.a6[0]+','+pos.a6[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />
      <polyline v-if="disflag.dis150 && getBottomUserInfo[0] > 1 && getBottomUserInfo[7] > 1" stroke-linecap="round"
        :points="pos.a1[0]+','+pos.a1[1]+' '+pos.a8[0]+','+pos.a8[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />

      <polyline v-if="disflag.dis150 && getBottomUserInfo[1] > 1 && getBottomUserInfo[6] > 1" stroke-linecap="round"
        :points="pos.a2[0]+','+pos.a2[1]+' '+pos.a7[0]+','+pos.a7[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />
      <polyline v-if="disflag.dis150 && getBottomUserInfo[1] > 1 && getBottomUserInfo[8] > 1" stroke-linecap="round"
        :points="pos.a2[0]+','+pos.a2[1]+' '+pos.a9[0]+','+pos.a9[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />

      <polyline v-if="disflag.dis150 && getBottomUserInfo[2] > 1 && getBottomUserInfo[7] > 1" stroke-linecap="round"
        :points="pos.a3[0]+','+pos.a3[1]+' '+pos.a8[0]+','+pos.a8[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />
      <polyline v-if="disflag.dis150 && getBottomUserInfo[2] > 1 && getBottomUserInfo[9] > 1" stroke-linecap="round"
        :points="pos.a3[0]+','+pos.a3[1]+' '+pos.a10[0]+','+pos.a10[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />

      <polyline v-if="disflag.dis150 && getBottomUserInfo[3] > 1 && getBottomUserInfo[8] > 1" stroke-linecap="round"
        :points="pos.a4[0]+','+pos.a4[1]+' '+pos.a9[0]+','+pos.a9[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />
      <polyline v-if="disflag.dis150 && getBottomUserInfo[3] > 1 && getBottomUserInfo[10] > 1" stroke-linecap="round"
        :points="pos.a4[0]+','+pos.a4[1]+' '+pos.a11[0]+','+pos.a11[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />

      <polyline v-if="disflag.dis150 && getBottomUserInfo[4] > 1 && getBottomUserInfo[9] > 1" stroke-linecap="round"
        :points="pos.a5[0]+','+pos.a5[1]+' '+pos.a10[0]+','+pos.a10[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />
      <polyline v-if="disflag.dis150 && getBottomUserInfo[4] > 1 && getBottomUserInfo[11] > 1" stroke-linecap="round"
        :points="pos.a5[0]+','+pos.a5[1]+' '+pos.a12[0]+','+pos.a12[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />

      <polyline v-if="disflag.dis150 && getBottomUserInfo[5] > 1 && getBottomUserInfo[10] > 1" stroke-linecap="round"
        :points="pos.a6[0]+','+pos.a6[1]+' '+pos.a11[0]+','+pos.a11[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />
      <polyline v-if="disflag.dis150 && getBottomUserInfo[5] > 1 && getBottomUserInfo[0] > 1" stroke-linecap="round"
        :points="pos.a6[0]+','+pos.a6[1]+' '+pos.a1[0]+','+pos.a1[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />

      <polyline v-if="disflag.dis150 && getBottomUserInfo[6] > 1 && getBottomUserInfo[11] > 1" stroke-linecap="round"
        :points="pos.a7[0]+','+pos.a7[1]+' '+pos.a12[0]+','+pos.a12[1]" style="fill:none;stroke:rgba(255, 120, 30, 0.5);stroke-width:3" />

        <!-- 60 -->
      <polyline v-if="disflag.dis60 && getBottomUserInfo[0] > 1 && getBottomUserInfo[10] > 1" stroke-linecap="round"
        :points="pos.a1[0]+','+pos.a1[1]+' '+pos.a11[0]+','+pos.a11[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[0] > 1 && getBottomUserInfo[2] > 1" stroke-linecap="round"
        :points="pos.a1[0]+','+pos.a1[1]+' '+pos.a3[0]+','+pos.a3[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[1] > 1 && getBottomUserInfo[11] > 1" stroke-linecap="round"
        :points="pos.a2[0]+','+pos.a2[1]+' '+pos.a12[0]+','+pos.a12[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[1] > 1 && getBottomUserInfo[3] > 1" stroke-linecap="round"
        :points="pos.a2[0]+','+pos.a2[1]+' '+pos.a4[0]+','+pos.a4[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[2] > 1 && getBottomUserInfo[0] > 1" stroke-linecap="round"
        :points="pos.a3[0]+','+pos.a3[1]+' '+pos.a1[0]+','+pos.a1[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[2] > 1 && getBottomUserInfo[4] > 1" stroke-linecap="round"
        :points="pos.a3[0]+','+pos.a3[1]+' '+pos.a5[0]+','+pos.a5[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[3] > 1 && getBottomUserInfo[1] > 1" stroke-linecap="round"
        :points="pos.a4[0]+','+pos.a4[1]+' '+pos.a2[0]+','+pos.a2[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[3] > 1 && getBottomUserInfo[5] > 1" stroke-linecap="round"
        :points="pos.a4[0]+','+pos.a4[1]+' '+pos.a6[0]+','+pos.a6[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[4] > 1 && getBottomUserInfo[2] > 1" stroke-linecap="round"
        :points="pos.a5[0]+','+pos.a5[1]+' '+pos.a3[0]+','+pos.a3[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[4] > 1 && getBottomUserInfo[6] > 1" stroke-linecap="round"
        :points="pos.a5[0]+','+pos.a5[1]+' '+pos.a7[0]+','+pos.a7[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[5] > 1 && getBottomUserInfo[3] > 1" stroke-linecap="round"
        :points="pos.a6[0]+','+pos.a6[1]+' '+pos.a4[0]+','+pos.a4[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[5] > 1 && getBottomUserInfo[7] > 1" stroke-linecap="round"
        :points="pos.a6[0]+','+pos.a6[1]+' '+pos.a8[0]+','+pos.a8[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[6] > 1 && getBottomUserInfo[4] > 1" stroke-linecap="round"
        :points="pos.a7[0]+','+pos.a7[1]+' '+pos.a5[0]+','+pos.a5[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[6] > 1 && getBottomUserInfo[8] > 1" stroke-linecap="round"
        :points="pos.a7[0]+','+pos.a7[1]+' '+pos.a9[0]+','+pos.a9[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[7] > 1 && getBottomUserInfo[5] > 1" stroke-linecap="round"
        :points="pos.a8[0]+','+pos.a8[1]+' '+pos.a6[0]+','+pos.a6[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[7] > 1 && getBottomUserInfo[9] > 1" stroke-linecap="round"
        :points="pos.a8[0]+','+pos.a8[1]+' '+pos.a10[0]+','+pos.a10[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[8] > 1 && getBottomUserInfo[6] > 1" stroke-linecap="round"
        :points="pos.a9[0]+','+pos.a9[1]+' '+pos.a7[0]+','+pos.a7[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[8] > 1 && getBottomUserInfo[10] > 1" stroke-linecap="round"
        :points="pos.a9[0]+','+pos.a9[1]+' '+pos.a11[0]+','+pos.a11[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[9] > 1 && getBottomUserInfo[7] > 1" stroke-linecap="round"
        :points="pos.a10[0]+','+pos.a10[1]+' '+pos.a8[0]+','+pos.a8[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[9] > 1 && getBottomUserInfo[11] > 1" stroke-linecap="round"
        :points="pos.a10[0]+','+pos.a10[1]+' '+pos.a12[0]+','+pos.a12[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[10] > 1 && getBottomUserInfo[8] > 1" stroke-linecap="round"
        :points="pos.a11[0]+','+pos.a11[1]+' '+pos.a9[0]+','+pos.a9[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[10] > 1 && getBottomUserInfo[0] > 1" stroke-linecap="round"
        :points="pos.a11[0]+','+pos.a11[1]+' '+pos.a1[0]+','+pos.a1[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />

      <polyline v-if="disflag.dis60 && getBottomUserInfo[11] > 1 && getBottomUserInfo[9] > 1" stroke-linecap="round"
        :points="pos.a12[0]+','+pos.a12[1]+' '+pos.a10[0]+','+pos.a10[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
      <polyline v-if="disflag.dis60 && getBottomUserInfo[11] > 1 && getBottomUserInfo[1] > 1" stroke-linecap="round"
        :points="pos.a12[0]+','+pos.a12[1]+' '+pos.a2[0]+','+pos.a2[1]" style="fill:none;stroke:#00ffc3;stroke-width:3" />
        <!-- 6 -->
      <polyline v-if="getBottomUserInfo[6] > 1 && getBottomUserInfo[7] > 1" stroke-linecap="round"
        :points="pos.a7[0]+','+pos.a7[1]+' '+pos.a8[0]+','+pos.a8[1]" style="fill:none;stroke:#dd2c00;stroke-width:20;opacity: 0.5" />
      <polyline v-if="getBottomUserInfo[3] > 1 && getBottomUserInfo[10] > 1" stroke-linecap="round"
        :points="pos.a4[0]+','+pos.a4[1]+' '+pos.a11[0]+','+pos.a11[1]" style="fill:none;stroke:#dd2c00;stroke-width:20;opacity: 0.5" />
      <polyline v-if="getBottomUserInfo[2] > 1 && getBottomUserInfo[11] > 1" stroke-linecap="round"
        :points="pos.a3[0]+','+pos.a3[1]+' '+pos.a12[0]+','+pos.a12[1]" style="fill:none;stroke:#00c853;stroke-width:20;opacity: 0.5" />
      <polyline v-if="getBottomUserInfo[1] > 1 && getBottomUserInfo[0] > 1" stroke-linecap="round"
        :points="pos.a2[0]+','+pos.a2[1]+' '+pos.a1[0]+','+pos.a1[1]" style="fill:none;stroke:#5f4339;stroke-width:20;opacity: 0.5" />
      <polyline v-if="getBottomUserInfo[4] > 1 && getBottomUserInfo[9] > 1" stroke-linecap="round"
        :points="pos.a5[0]+','+pos.a5[1]+' '+pos.a10[0]+','+pos.a10[1]" style="fill:none;stroke:#ffca05;stroke-width:20;opacity: 0.5" />
      <polyline v-if="getBottomUserInfo[5] > 1 && getBottomUserInfo[8] > 1" stroke-linecap="round"
        :points="pos.a6[0]+','+pos.a6[1]+' '+pos.a9[0]+','+pos.a9[1]" style="fill:none;stroke:#1976d2;stroke-width:20;opacity: 0.5" />

    </svg>
  </div>

  <div class="cbundle">
    <div class="citem" style="background-image: url('bottom/6.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[5]*2 +')'}"
         :class="getBottomUserInfo[5] > 1 ? 'citem-active2' : ''">
    </div>
    <div class="citem" style="background-image: url('bottom/7.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[6]*2 +')'}"
         :class="getBottomUserInfo[6] > 1 ? 'citem-active2' : ''">
    </div>
    <div class="citem" style="background-image: url('bottom/8.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[7]*2 +')'}"
         :class="getBottomUserInfo[7] > 1 ? 'citem-active2' : ''">
    </div>
    <div class="citem" style="background-image: url('bottom/9.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[8]*2 +')'}"
         :class="getBottomUserInfo[8] > 1 ? 'citem-active4' : ''">
    </div>
    <div class="citem" style="background-image: url('bottom/5.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[4]*2 +')'}"
         :class="getBottomUserInfo[4] > 1 ? 'citem-active1' : ''">
    </div>
    <div class="citem citemhide" style="grid-column: 2 / span 2;width: 114px;height: 114px;grid-row: 2 /span 2;"></div>
    <div class="citem" style="background-image: url('bottom/10.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[9]*2 +')'}"
         :class="getBottomUserInfo[9] > 1 ? 'citem-active4' : ''">
    </div>
    <div class="citem" style="background-image: url('bottom/4.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[3]*2 +')'}"
         :class="getBottomUserInfo[3] > 1 ? 'citem-active1' : ''">
    </div>
    <div class="citem" style="background-image: url('bottom/11.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[10]*2 +')'}"
         :class="getBottomUserInfo[10] > 1 ? 'citem-active4' : ''">
    </div>
    <div class="citem" style="background-image: url('bottom/3.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[2]*2 +')'}"
         :class="getBottomUserInfo[2] > 1 ? 'citem-active1' : ''">
    </div>
    <div class="citem" style="background-image: url('bottom/2.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[1]*2 +')'}"
         :class="getBottomUserInfo[1] > 1 ? 'citem-active5' : ''">
    </div>
    <div class="citem" style="background-image: url('bottom/1.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[0]*2 +')'}"
         :class="getBottomUserInfo[0] > 1 ? 'citem-active5' : ''">
    </div>
    <div class="citem" style="background-image: url('bottom/12.jpg')"
         :style="{transform: 'rotateZ(-'+rotate+'deg) scale(1.'+ getBottomUserInfo[11]*2 +')'}"
         :class="getBottomUserInfo[11] > 1 ? 'citem-active5' : ''">
    </div>
  </div>
</div>