export function getGodName(num, tengorType) {
  if (tengorType === 1) {
    switch (num) {
      case 1: return '純情'
      case 2: return '佛系'
      case 3: return '女皇'
      case 4: return '朋友'
      case 5: return '引人'
      case 6: return '重情'
      case 7: return '旺夫'
      case 8: return '咸濕'
      case 9: return '守己'
      case 10: return '愛被管'
      default: return ''
    }
  } else if (tengorType === 2) {
    switch (num) {
      case 1: return '父母'
      case 2: return '長輩'
      case 3: return '兄妹'
      case 4: return '兄妹'
      case 5: return '阿仔'
      case 6: return '阿女'
      case 7: return '下屬'
      case 8: return '阿爸'
      case 9: return '老公'
      case 10: return '情/小人'
      default: return ''
    }
  } else if (tengorType === 3) {
    switch (num) {
      case 1: return '皮膚'
      case 2: return '皮膚'
      case 3: return '四肢'
      case 4: return '四肢'
      case 5: return '口舌'
      case 6: return '口舌'
      case 7: return '呼吸'
      case 8: return '呼吸'
      case 9: return '外傷'
      case 10: return '外傷'
      default: return ''
    }
  } else if (tengorType === 4) {
    switch (num) {
      case 1: return '權印'
      case 2: return '文化'
      case 3: return '競爭'
      case 4: return '合作'
      case 5: return '創作'
      case 6: return '享樂'
      case 7: return '財物'
      case 8: return '旅行'
      case 9: return '公職'
      case 10: return '抗壓'
      default: return ''
    }
  } else if (tengorType === 6) {
    switch (num) {
      case 1: return '學習'
      case 2: return '思考'
      case 3: return '想洗錢'
      case 4: return '約食飯'
      case 5: return '溝仔仔'
      case 6: return '引仔仔'
      case 7: return '財運'
      case 8: return '食仔'
      case 9: return '結婚'
      case 10: return '比仔溝'
      default: return ''
    }
  } else if (tengorType === 7) {
    switch (num) {
      case 1: return '學野'
      case 2: return '修哲'
      case 3: return '分析'
      case 4: return '計劃合作'
      case 5: return '比仔溝'
      case 6: return '溝仔'
      case 7: return '儲錢'
      case 8: return '投資'
      case 9: return '結婚'
      case 10: return '偷食'
      default: return ''
    }
  } else if (tengorType === 8) {
    switch (num) {
      case 1: return '幸運'
      case 2: return '幸運'
      case 3: return '精神'
      case 4: return '精神'
      case 5: return '攻擊力'
      case 6: return '攻擊力'
      case 7: return '掉寶率'
      case 8: return '掉寶率'
      case 9: return '防禦'
      case 10: return '防禦'
      default: return ''
    }
  } else {
    switch (num) {
      case 1: return '正印'
      case 2: return '偏印'
      case 3: return '劫財'
      case 4: return '比肩'
      case 5: return '傷官'
      case 6: return '食神'
      case 7: return '正財'
      case 8: return '偏財'
      case 9: return '正官'
      case 10: return '七殺'
      default: return ''
    }
  }
}
export function getGodNameM(num, tengorType) {
  if (tengorType === 1) {
    switch (num) {
      case 1: return '純情'
      case 2: return '佛系'
      case 3: return '孤王'
      case 4: return '朋友'
      case 5: return '才華'
      case 6: return '冧女'
      case 7: return '重女'
      case 8: return '食女'
      case 9: return '正經'
      case 10: return '兇狠'
      default: return ''
    }
  } else if (tengorType === 2) {
    switch (num) {
      case 1: return '父母'
      case 2: return '長輩'
      case 3: return '兄妹'
      case 4: return '兄妹'
      case 5: return '阿仔'
      case 6: return '阿女'
      case 7: return '老婆'
      case 8: return '情人'
      case 9: return '上司'
      case 10: return '小人'
      default: return ''
    }
  } else if (tengorType === 3) {
    switch (num) {
      case 1: return '皮膚'
      case 2: return '皮膚'
      case 3: return '四肢'
      case 4: return '四肢'
      case 5: return '口舌'
      case 6: return '口舌'
      case 7: return '精血'
      case 8: return '精血'
      case 9: return '外傷'
      case 10: return '外傷'
      default: return ''
    }
  } else if (tengorType === 4) {
    switch (num) {
      case 1: return '權印'
      case 2: return '文化'
      case 3: return '競爭'
      case 4: return '合作'
      case 5: return '創作'
      case 6: return '享樂'
      case 7: return '財物'
      case 8: return '女人'
      case 9: return '公職'
      case 10: return '抗壓'
      default: return ''
    }
  } else if (tengorType === 6) {
    switch (num) {
      case 1: return '學習'
      case 2: return '思考'
      case 3: return '想洗錢'
      case 4: return '約食飯'
      case 5: return '屌柒人'
      case 6: return '比意見'
      case 7: return '財運'
      case 8: return '攬女'
      case 9: return '忙緊'
      case 10: return '受傷'
      default: return ''
    }
  } else if (tengorType === 7) {
    switch (num) {
      case 1: return '學野'
      case 2: return '修哲'
      case 3: return '分析'
      case 4: return '計劃合作'
      case 5: return '創作'
      case 6: return '溝女'
      case 7: return '結婚/儲錢'
      case 8: return '偷食/投資'
      case 9: return '升職'
      case 10: return '乜都無做'
      default: return ''
    }
  } else if (tengorType === 8) {
    switch (num) {
      case 1: return '幸運'
      case 2: return '幸運'
      case 3: return '精神'
      case 4: return '精神'
      case 5: return '攻擊力'
      case 6: return '攻擊力'
      case 7: return '掉寶率'
      case 8: return '掉寶率'
      case 9: return '防禦'
      case 10: return '防禦'
      default: return ''
    }
  } else {
    switch (num) {
      case 1: return '正印'
      case 2: return '偏印'
      case 3: return '劫財'
      case 4: return '比肩'
      case 5: return '傷官'
      case 6: return '食神'
      case 7: return '正財'
      case 8: return '偏財'
      case 9: return '正官'
      case 10: return '七殺'
      default: return ''
    }
  }
}
export function convertGod(day, sex, type) {
  if (sex) {
    switch (day) {
      case 1: return [0, getGodNameM(4, type), getGodNameM(3, type), getGodNameM(6, type), getGodNameM(5, type), getGodNameM(8, type), getGodNameM(7, type), getGodNameM(10, type), getGodNameM(9, type), getGodNameM(2, type), getGodNameM(1, type)]
      case 2: return [0, getGodNameM(3, type), getGodNameM(4, type), getGodNameM(5, type), getGodNameM(6, type), getGodNameM(7, type), getGodNameM(8, type), getGodNameM(9, type), getGodNameM(10, type), getGodNameM(1, type), getGodNameM(2, type)]
      case 3: return [0, getGodNameM(2, type), getGodNameM(1, type), getGodNameM(4, type), getGodNameM(3, type), getGodNameM(6, type), getGodNameM(5, type), getGodNameM(8, type), getGodNameM(7, type), getGodNameM(10, type), getGodNameM(9, type)]
      case 4: return [0, getGodNameM(1, type), getGodNameM(2, type), getGodNameM(3, type), getGodNameM(4, type), getGodNameM(5, type), getGodNameM(6, type), getGodNameM(7, type), getGodNameM(8, type), getGodNameM(9, type), getGodNameM(10, type)]
      case 5: return [0, getGodNameM(10, type), getGodNameM(9, type), getGodNameM(2, type), getGodNameM(1, type), getGodNameM(4, type), getGodNameM(3, type), getGodNameM(6, type), getGodNameM(5, type), getGodNameM(8, type), getGodNameM(7, type)]
      case 6: return [0, getGodNameM(9, type), getGodNameM(10, type), getGodNameM(1, type), getGodNameM(2, type), getGodNameM(3, type), getGodNameM(4, type), getGodNameM(5, type), getGodNameM(6, type), getGodNameM(7, type), getGodNameM(8, type)]
      case 7: return [0, getGodNameM(8, type), getGodNameM(7, type), getGodNameM(10, type), getGodNameM(9, type), getGodNameM(2, type), getGodNameM(1, type), getGodNameM(4, type), getGodNameM(3, type), getGodNameM(6, type), getGodNameM(5, type)]
      case 8: return [0, getGodNameM(7, type), getGodNameM(8, type), getGodNameM(9, type), getGodNameM(10, type), getGodNameM(1, type), getGodNameM(2, type), getGodNameM(3, type), getGodNameM(4, type), getGodNameM(5, type), getGodNameM(6, type)]
      case 9: return [0, getGodNameM(6, type), getGodNameM(5, type), getGodNameM(8, type), getGodNameM(7, type), getGodNameM(10, type), getGodNameM(9, type), getGodNameM(2, type), getGodNameM(1, type), getGodNameM(4, type), getGodNameM(3, type)]
      case 10: return [0, getGodNameM(5, type), getGodNameM(6, type), getGodNameM(7, type), getGodNameM(8, type), getGodNameM(9, type), getGodNameM(10, type), getGodNameM(1, type), getGodNameM(2, type), getGodNameM(3, type), getGodNameM(4, type)]
      default: return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }
  } else {
    switch (day) {
      case 1: return [0, getGodName(4, type), getGodName(3, type), getGodName(6, type), getGodName(5, type), getGodName(8, type), getGodName(7, type), getGodName(10, type), getGodName(9, type), getGodName(2, type), getGodName(1, type)]
      case 2: return [0, getGodName(3, type), getGodName(4, type), getGodName(5, type), getGodName(6, type), getGodName(7, type), getGodName(8, type), getGodName(9, type), getGodName(10, type), getGodName(1, type), getGodName(2, type)]
      case 3: return [0, getGodName(2, type), getGodName(1, type), getGodName(4, type), getGodName(3, type), getGodName(6, type), getGodName(5, type), getGodName(8, type), getGodName(7, type), getGodName(10, type), getGodName(9, type)]
      case 4: return [0, getGodName(1, type), getGodName(2, type), getGodName(3, type), getGodName(4, type), getGodName(5, type), getGodName(6, type), getGodName(7, type), getGodName(8, type), getGodName(9, type), getGodName(10, type)]
      case 5: return [0, getGodName(10, type), getGodName(9, type), getGodName(2, type), getGodName(1, type), getGodName(4, type), getGodName(3, type), getGodName(6, type), getGodName(5, type), getGodName(8, type), getGodName(7, type)]
      case 6: return [0, getGodName(9, type), getGodName(10, type), getGodName(1, type), getGodName(2, type), getGodName(3, type), getGodName(4, type), getGodName(5, type), getGodName(6, type), getGodName(7, type), getGodName(8, type)]
      case 7: return [0, getGodName(8, type), getGodName(7, type), getGodName(10, type), getGodName(9, type), getGodName(2, type), getGodName(1, type), getGodName(4, type), getGodName(3, type), getGodName(6, type), getGodName(5, type)]
      case 8: return [0, getGodName(7, type), getGodName(8, type), getGodName(9, type), getGodName(10, type), getGodName(1, type), getGodName(2, type), getGodName(3, type), getGodName(4, type), getGodName(5, type), getGodName(6, type)]
      case 9: return [0, getGodName(6, type), getGodName(5, type), getGodName(8, type), getGodName(7, type), getGodName(10, type), getGodName(9, type), getGodName(2, type), getGodName(1, type), getGodName(4, type), getGodName(3, type)]
      case 10: return [0, getGodName(5, type), getGodName(6, type), getGodName(7, type), getGodName(8, type), getGodName(9, type), getGodName(10, type), getGodName(1, type), getGodName(2, type), getGodName(3, type), getGodName(4, type)]
      default: return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }
  }
}
