function getCArea(num) {
  switch (num) {
    case 1:
      console.log(`局數:${1}`)
      return [5, 6, 7, 8, 9, 10, 4, 3, 2]
    case 2:
      console.log(`局數:${2}`)
      return [2, 5, 6, 7, 8, 9, 10, 4, 3]
    case 3:
      console.log(`局數:${3}`)
      return [3, 2, 5, 6, 7, 8, 9, 10, 4]
    case 4:
      console.log(`局數:${4}`)
      return [4, 3, 2, 5, 6, 7, 8, 9, 10]
    case 5:
      console.log(`局數:${5}`)
      return [10, 4, 3, 2, 5, 6, 7, 8, 9]
    case 6:
      console.log(`局數:${6}`)
      return [9, 10, 4, 3, 2, 5, 6, 7, 8]
    case 7:
      console.log(`局數:${7}`)
      return [8, 9, 10, 4, 3, 2, 5, 6, 7]
    case 8:
      console.log(`局數:${8}`)
      return [7, 8, 9, 10, 4, 3, 2, 5, 6]
    case 9:
      console.log(`局數:${9}`)
      return [6, 7, 8, 9, 10, 4, 3, 2, 5]
    default: return [0, 0, 0, 0, 0, 0, 0, 0, 0]
  }
}

function getTArea(num) {
  switch (num) {
    case 2:
      return [5, 6, 7, 8, 9, 10, 4, 3, 2]
    case 3:
      return [2, 5, 6, 7, 8, 9, 10, 4, 3]
    case 4:
      return [3, 2, 5, 6, 7, 8, 9, 10, 4]
    case 10:
      return [4, 3, 2, 5, 6, 7, 8, 9, 10]
    case 9:
      return [10, 4, 3, 2, 5, 6, 7, 8, 9]
    case 8:
      return [9, 10, 4, 3, 2, 5, 6, 7, 8]
    case 7:
      return [8, 9, 10, 4, 3, 2, 5, 6, 7]
    case 6:
      return [7, 8, 9, 10, 4, 3, 2, 5, 6]
    case 5:
      return [6, 7, 8, 9, 10, 4, 3, 2, 5]
    default: return [0, 0, 0, 0, 0, 0, 0, 0, 0]
  }
}

function getArrayNum() {
  // 冬至
  return [
    [8, 5, 2],
    [9, 6, 3],
    [1, 7, 4],

    [3, 9, 6],
    [4, 1, 7],
    [5, 2, 8],

    [4, 1, 7],
    [5, 2, 8],
    [6, 3, 9],

    [9, 3, 6],
    [8, 2, 5],
    [7, 1, 4],

    [2, 5, 8],
    [1, 4, 7],
    [9, 3, 6],

    [7, 1, 4],
    [6, 9, 3],
    [5, 8, 2],

    [6, 9, 3],
    [5, 8, 2],
    [4, 7, 1],

    [1, 7, 4],
    [2, 8, 5],
    [3, 9, 6],
  ]
}

function genArrayNumListBySpring(arr) {
  const res = []
  for (let i = 0; i < arr.length; i += 1) {
    for (let j = 0; j < arr[i].length; j += 1) {
      res.push(arr[i][j])
      res.push(arr[i][j])
      res.push(arr[i][j])
      res.push(arr[i][j])
      res.push(arr[i][j])
    }
  }
  return res
}

function getDateArrayNum(date) {
  const oneDay = 1000 * 60 * 60 * 24
  const yr = date.getFullYear()
  const mm = date.getMonth()
  const dd = date.getDate()
  const tgdate = new Date(yr, mm, dd)
  // var tgdate = new Date(yr , 2 - 1, 9);
  const springDate = new Date(yr, 2 - 1, 4)
  let diff2 = 0
  let day2 = 0
  let flag = 0
  console.log(`Day of year: ${day2}`)

  if (tgdate > springDate) {
    diff2 = tgdate - springDate
    day2 = Math.floor(diff2 / oneDay)
    flag = 0
  } else if (tgdate == springDate) {
    flag = 1
  } else {
    diff2 = springDate - tgdate
    day2 = Math.floor(diff2 / oneDay)
    flag = 2
  }

  const lst = genArrayNumListBySpring(getArrayNum())
  if (flag == 2) {
    return lst[lst.length - day2]
  }
  return lst[day2]
}

function checkFL(date) {
  const curYear = date
  const yr = date.getFullYear()
  // var springDate = new Date(yr + '-' + '02' + '-' + '04')
  const summerDate = new Date(`${yr}-` + '06' + '-' + '21')
  const winterDate = new Date(`${yr}-` + '12' + '-' + '21')
  if (curYear.getTime() > summerDate.getTime()
      && curYear.getTime() < winterDate.getTime()) {
    console.log('陰遁')
    return false
  }
  console.log('陽遁')
  return true
}

function genArea(arr, date) {
  if (checkFL(date)) {
    return [
      arr[3], arr[8], arr[1],
      arr[2], arr[4], arr[6],
      arr[7], arr[0], arr[5],
    ]
  }
  return [
    arr[5], arr[0], arr[7],
    arr[6], arr[4], arr[2],
    arr[1], arr[8], arr[3],
  ]
}

function topName(num) {
  switch (num) {
    case 2: return '乙'
    case 3: return '丙'
    case 4: return '丁'
    case 5: return '戊'
    case 6: return '己'
    case 7: return '庚'
    case 8: return '辛'
    case 9: return '壬'
    case 10: return '癸'
    default: return ''
  }
}

function getBValList(position, arr) {
  switch (position) {
    case 0:
      return [arr[1], arr[2], arr[5], arr[8], arr[7], arr[6], arr[3]]
    case 1:
      return [arr[2], arr[5], arr[8], arr[7], arr[6], arr[3], arr[0]]
    case 2:
      return [arr[5], arr[8], arr[7], arr[6], arr[3], arr[0], arr[1]]
    case 3:
      return [arr[0], arr[1], arr[2], arr[5], arr[8], arr[7], arr[6]]
    case 4:
      return [arr[5], arr[8], arr[7], arr[6], arr[3], arr[0], arr[1]]
    case 5:
      return [arr[8], arr[7], arr[6], arr[3], arr[0], arr[1], arr[2]]
    case 6:
      return [arr[3], arr[0], arr[1], arr[2], arr[5], arr[8], arr[7]]
    case 7:
      return [arr[6], arr[3], arr[0], arr[1], arr[2], arr[5], arr[8]]
    case 8:
      return [arr[7], arr[6], arr[3], arr[0], arr[1], arr[2], arr[5]]
    default:
      return [0, 0, 0, 0, 0, 0, 0]
  }
}

function getTArray(arr, num, ficon, dtVal) {
  // 天地盤
  let position = 0
  let bposition = 0
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i] === dtVal) {
      position = i
    }
    if (arr[i] === ficon) {
      bposition = i
    }
  }
  const tLst = getBValList(bposition, arr)
  switch (position) {
    case 0:
      return [
        ficon, tLst[0], tLst[1],
        tLst[6], 0, tLst[2],
        tLst[5], tLst[4], tLst[3],
      ]
    case 1:
      return [
        tLst[6], ficon, tLst[0],
        tLst[5], 0, tLst[1],
        tLst[4], tLst[3], tLst[2],
      ]
    case 2:
      return [
        tLst[5], tLst[6], ficon,
        tLst[4], 0, tLst[0],
        tLst[3], tLst[2], tLst[1],
      ]
    case 3:
      return [
        tLst[0], tLst[1], tLst[2],
        ficon, 0, tLst[3],
        tLst[6], tLst[5], tLst[4],
      ]
    case 4:
      return [
        tLst[5], tLst[6], ficon,
        tLst[4], 0, tLst[0],
        tLst[3], tLst[2], tLst[1],
      ]
    case 5:
      return [
        tLst[4], tLst[5], tLst[6],
        tLst[3], 0, ficon,
        tLst[2], tLst[1], tLst[0],
      ]
    case 6:
      return [
        tLst[1], tLst[2], tLst[3],
        tLst[0], 0, tLst[4],
        ficon, tLst[6], tLst[5],
      ]
    case 7:
      return [
        tLst[2], tLst[3], tLst[4],
        tLst[1], 0, tLst[5],
        tLst[0], ficon, tLst[6],
      ]
    case 8:
      return [
        tLst[3], tLst[4], tLst[5],
        tLst[2], 0, tLst[6],
        tLst[1], tLst[0], ficon,
      ]
    default:
      return [
        0, 0, 0,
        0, 0, 0,
        0, 0, 0,
      ]
  }
}

function getTArrayPosition(arr, dtVal) {
  // 地盤宮位
  let position = 0
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i] === dtVal) {
      position = i
    }
  }
  if (position === 4) {
    return 2
  }
  return position
}

function getGodArrayList(date, num) {
  const god = getGodLst()
  if (checkFL(date)) {
    switch (num) {
      case 0:
        return [
          god[0], god[1], god[2],
          god[7], null, god[3],
          god[6], god[5], god[4],
        ]
      case 1:
        return [
          god[7], god[0], god[1],
          god[6], null, god[2],
          god[5], god[4], god[3],
        ]
      case 2:
        return [
          god[6], god[7], god[0],
          god[5], null, god[1],
          god[4], god[3], god[2],
        ]
      case 3:
        return [
          god[1], god[2], god[3],
          god[0], null, god[4],
          god[7], god[6], god[5],
        ]
      case 4:

      case 5:
        return [
          god[5], god[6], god[7],
          god[4], null, god[0],
          god[3], god[2], god[1],
        ]
      case 6:
        return [
          god[2], god[3], god[4],
          god[1], null, god[5],
          god[0], god[7], god[6],
        ]
      case 7:
        return [
          god[3], god[4], god[5],
          god[2], null, god[6],
          god[1], god[0], god[7],
        ]
      case 8:
        return [
          god[4], god[5], god[6],
          god[3], null, god[7],
          god[2], god[1], god[0],
        ]
      default:
        return [
          null, null, null,
          null, null, null,
          null, null, null,
        ]
    }
  } else {
    switch (num) {
      case 0:
        return [
          god[0], god[7], god[6],
          god[1], null, god[5],
          god[2], god[3], god[4],
        ]
      case 1:
        return [
          god[1], god[0], god[7],
          god[2], null, god[6],
          god[3], god[4], god[5],
        ]
      case 2:
        return [
          god[2], god[1], god[0],
          god[3], null, god[7],
          god[4], god[5], god[6],
        ]
      case 3:
        return [
          god[7], god[6], god[5],
          god[0], null, god[4],
          god[1], god[2], god[3],
        ]

      case 5:
        return [
          god[3], god[2], god[1],
          god[4], null, god[0],
          god[5], god[6], god[7],
        ]
      case 6:
        return [
          god[6], god[5], god[4],
          god[7], null, god[3],
          god[0], god[1], god[2],
        ]
      case 7:
        return [
          god[5], god[4], god[3],
          god[6], null, god[2],
          god[7], god[0], god[1],
        ]
      case 8:
        return [
          god[4], god[3], god[2],
          god[5], null, god[1],
          god[6], god[7], god[0],
        ]
      default:
        return [
          null, null, null,
          null, null, null,
          null, null, null,
        ]
    }
  }
}

function outResult(arr, num, ficon, dtVal, dbVal, date) {
  const decideLst = getDecideType(num)

  const tarraylst = getTArray(arr, num, ficon, dtVal)
  const barray = getTArrayPosition(arr, dtVal)
  const godLst = getGodArrayList(date, barray)
  console.log(godLst[0], godLst[1], godLst[2])
  console.log(godLst[3], godLst[4], godLst[5])
  console.log(godLst[6], godLst[7], godLst[8])
  console.log('--------------------------')
  console.log(topName(tarraylst[0]), topName(tarraylst[1]), topName(tarraylst[2]))
  console.log(topName(tarraylst[3]), topName(tarraylst[4]), topName(tarraylst[5]))
  console.log(topName(tarraylst[6]), topName(tarraylst[7]), topName(tarraylst[8]))
  console.log('--------------------------')
  console.log(topName(arr[0]), topName(arr[1]), topName(arr[2]))
  console.log(topName(arr[3]), topName(arr[4]), topName(arr[5]))
  console.log(topName(arr[6]), topName(arr[7]), topName(arr[8]))
  console.log('--------------------------')
}

function getDecideType(num) {
  // 1. 天文 2.地理 3.人物 4.身體 5.性情 6.事業 7.屋舍 8.飲食 9.物品
  switch (num) {
    case 1:
      return getDecideArr(1)
    case 2:
      return getDecideArr(2)
    case 3:
      return getDecideArr(3)
    case 4:
      return getDecideArr(4)
    case 5:
      return getDecideArr(5)
    case 6:
      return getDecideArr(6)
    case 7:
      return getDecideArr(7)
    case 8:
      return getDecideArr(8)
    case 9:
      return getDecideArr(9)
    default:
      return getDecideArr(0)
  }
}

function getDecideArr(num) {
  switch (num) {
    case 1:
      return [
        '',
        '太陽',
        '月亮',
        '木星',
        '計星',
        '水星',
        '金星',
        '氣星',
        '土星',
        '火星',
        '羅星',
      ]
    case 2:
      return [
        '',
        '高地、高山、樹林',
        '草原、花園',
        '首都、乾旱之地',
        '燒過之地、繁華之地',
        '平廣之地、堤防',
        '田地、平原',
        '池塘、礦場',
        '荒地、墳墓',
        '沙場、戰場',
        '黑暗地、黃泉',
      ]
    case 3:
      return [
        '',
        '總統、國王、官吏',
        '皇后、總統夫人、船員、旅行者',
        '將軍、教宗、首相、總理',
        '官吏、相卜者、司機',
        '詩人、教師、大使',
        '歌星、舞女、妓女',
        '巫女、間諜、嗜酒者',
        '農民、木工、教徒',
        '戰士、盜賊、廚子',
        '屍首、兇手、間諜、偵探',
      ]
    case 4:
      return [
        '',
        '肝膽、眼睛、筋',
        '肝臟、眼睛、手爪',
        '小腸、脈、唇',
        '心、唇、氣',
        '胃、舌、肉',
        '脾、舌、脂',
        '大腸、鼻子、皮膚',
        '肺、鼻、毛',
        '膀胱、耳朵、骨頭',
        '腎臟、頭髮、耳朵',
      ]
    case 5:
      return [
        '',
        '威嚴、正直、愉快、頑固、獨斷、浪費',
        '敏感、幻想、忍耐、依賴、懦弱、自私',
        '寬大、慈悲、正義、短見、虛榮、偏激',
        '友善、上進、孤立、暴燥、叛逆',
        '機智、順應、雄辯、狡滑、虛僞、焦慮',
        '溫和、忠實、通融、優柔寡斷、懦弱、怠惰',
        '多情、理想、疏忽、不安、欺騙',
        '實在、中庸、耐久、苛薄、冷淡、憂鬱',
        '積極、勇敢、熱情粗野、急性',
        '活動、啓發、強制、不忠實',
      ]
    case 6:
      return [
        '',
        '藝術、實業、企業',
        '航海家、玄秘之術',
        '學術、政治',
        '命相卜業',
        '醫生、商賈',
        '美術、戲劇業',
        '畜牧業、狩獵業',
        '道術、武術',
        '軍職、屠宰業',
        '員警、調查員',
      ]
    case 7:
      return [
        '',
        '宮殿、高塔',
        '集會所、餐廳',
        '法庭、高樓大廈',
        '圖書室、書店、畜舍',
        '學校、福利社、商店',
        '閨房、戲院',
        '墓地、仙幻之境',
        '寺廟',
        '軍營、牢房',
        '洞穴、陰府',
      ]
    case 8:
      return [
        '',
        '酸姓食物、幹美食物',
        '澀味之物、一般食物',
        '苦味之物、水果類',
        '香濃之物、燒烤之物',
        '鮮美之物',
        '穀物、甜美之物',
        '辛辣之物、甘美之物',
        '辛辣之物、鹵醬之物',
        '鹹物、煎過之物',
        '醬漬過之物、淡色之物',
      ]
    case 9:
      return [
        '',
        '王冠、金、玉、寶物、青色物',
        '水銀、遺失物、日用品、綠色之物',
        '藥石、鋅、錫、商品、紅色之物',
        '鉛、剪刀、車輛、紅色之物',
        '書本、筆墨、文具、黃色之物',
        '衣服、戒指、橙色之物',
        '鋼鐵、刀刃、白色之物',
        '斧、錘、弓、箭、透明之物',
        '槍炮、火雷、燈燭、黑色之物',
        '冥紙、棺木、紫色之物',
      ]
    default:
      return ['', '', '', '', '', '', '', '', '', '']
  }
}

function getFirstIcon(num1, num2) {
  // EG丙申
  const stepnum = num1 - 1
  const lst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
  const res = ((num2 + 12) - stepnum) % 12
  switch (res) {
    case 1: return 5
    case 11: return 6
    case 9: return 7
    case 7: return 8
    case 5: return 9
    case 3: return 10
    default: return 0
  }
}

function getGodLst() {
  const obj = [
    {
      id: 1,
      name: '值',
    },
    {
      id: 2,
      name: '蛇',
    },
    {
      id: 3,
      name: '月',
    },
    {
      id: 4,
      name: '合',
    },
    {
      id: 5,
      name: '白',
    },
    {
      id: 6,
      name: '玄',
    },
    {
      id: 7,
      name: '地',
    },
    {
      id: 8,
      name: '天',
    },
  ]

  return obj
}

function getStarLst() {
  const obj = [
    {
      id: 1,
      name: '蓬',
      star: '貪狼',
      arr: 1,
      good: '訟庭爭',
      bad: '宜嫁娶，營造，搬遷',
    },
    {
      id: 2,
      name: '任',
      star: '左輔',
      arr: 8,
      good: '皆通',
      bad: 'X',
    },
    {
      id: 3,
      name: '沖',
      star: '祿存',
      arr: 3,
      good: '出軍報仇雪恥',
      bad: '嫁娶，修造',
    },
    {
      id: 4,
      name: '輔',
      star: '文曲',
      arr: 4,
      good: '升學 發展',
      bad: 'X',
    },
    {
      id: 5,
      name: '禽',
      star: '廉貞',
      arr: 5,
      good: '百事皆宜',
      bad: 'X',
    },
    {
      id: 6,
      name: '英',
      star: '右弼',
      arr: 9,
      good: '謀劃獻策',
      bad: '血光之災 嫁娶 遠行移徒 上官商賈',
    },
    {
      id: 7,
      name: '芮',
      star: '巨門',
      arr: 2,
      good: '受道結交友 受業師長、屯兵固守',
      bad: '嫁娶，遷徙，訴訟',
    },
    {
      id: 8,
      name: '柱',
      star: '破軍',
      arr: 7,
      good: '隱士，宜隱跡固守 訓練',
      bad: '出戰交兵，經商遠行 傷災',
    },
    {
      id: 9,
      name: '心',
      star: '武曲',
      arr: 6,
      good: '治病服藥，練氣功，經商',
      bad: '~~',
    },
  ]
  return obj
}

function getDoorLst() {
  // 所謂出吉門系指往當日吉門的方向行走15分鐘以上才會有效果
  // 臨兵鬥者皆陣列在前
  const obj = [
    {
      door: 1,
      name: '開門',
      desc: '遠行 上任、求財、婚姻嫁娶、訪友 公開',
      type: true,
      best: '公開',
      org: 4,
    },
    {
      door: 2,
      name: '休門',
      desc: '輕鬆 活動 求財 婚姻嫁娶 遠行 新官上任',
      type: true,
      best: '玩',
      org: 5,
    },
    {
      door: 3,
      name: '生門',
      desc: '求財作生意 求職 婚姻 求醫',
      type: true,
      best: '求財',
      org: 3,
    },
    {
      door: 4,
      name: '傷門',
      desc: '破壞 見血光 釣魚打獵 博戲 索債或圍捕盜賊 利刑事訴訟',
      type: false,
      best: '追擊',
      org: 1,
    },
    {
      door: 5,
      name: '杜門',
      desc: '隱藏 躲災避難',
      type: false,
      best: '隱藏',
      org: 1,
    },
    {
      door: 6,
      name: '景門',
      desc: '利考試 廣告宣傳 婚姻嫁娶',
      type: true,
      best: '考試',
      org: 2,
    },
    {
      door: 7,
      name: '驚門',
      desc: '驚恐怪異 遇驚慌恐亂 利民事訴訟',
      type: false,
      best: '索債',
      org: 3,
    },
    {
      door: 8,
      name: '死門',
      desc: '弔喪捕獵',
      type: false,
      best: '死',
      org: 4,
    },
  ]
  return obj
}

export function Test() {
  const type = 9
  // 失物
  const ficon = getFirstIcon(4, 10)
  // 丙申
  const a = new Date(2020, 0, 14)
  outResult(genArea(getCArea(getDateArrayNum(a)), a), type, ficon, 4, 10, a)
}

Test()
