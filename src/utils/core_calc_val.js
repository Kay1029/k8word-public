export function calcNumofPower(num1, num2, num3, num4, num5, num) {
  switch (num) {
    case 1:
    case 2:
      return [Math.floor(num5 + num1), Math.floor(num2 + num3 + num4)]
    case 3:
    case 4:
      return [Math.floor(num1 + num2), Math.floor(num5 + num3 + num4)]
    case 5:
    case 6:
      return [Math.floor(num2 + num3), Math.floor(num1 + num5 + num4)]
    case 7:
    case 8:
      return [Math.floor(num3 + num4), Math.floor(num2 + num1 + num5)]
    case 9:
    case 10:
      return [Math.floor(num4 + num5), Math.floor(num2 + num3 + num1)]
    default:
      return [0, 0]
  }
}

export function calcWordValue(num1, num2) {
  // 生系數
  // const urate = 1.5
  const uurate = 1.0
  const lurate = 0.8
  // 剋系數
  // const urate = 1.5
  // const uurate = 0.5
  // const lurate = 0.8
  const a = num1[0]
  const b = num2
  for (let i = 0; i < b.length; i += 1) {
    if ((a.num === 1 || a.num === 2) && (b[i].num === 9 || b[i].num === 10)) {
      a.value += b[i].value
      b[i].value *= lurate
    }
    if ((a.num === 1 || a.num === 2) && (b[i].num === 1 || b[i].num === 2)) {
      a.value = (a.value + b[i].value) * uurate
      b[i].value *= uurate
      // b[i].value *= lurate
    }
    if ((a.num === 1 || a.num === 2) && (b[i].num === 3 || b[i].num === 4)) {
      a.value -= (b[i].value / 2)
      b[i].value *= uurate
    }
    if ((a.num === 1 || a.num === 2) && (b[i].num === 5 || b[i].num === 6 || b[i].num === 7 || b[i].num === 8)) {
      a.value -= (b[i].value / 2)
      b[i].value *= lurate
    }
    a.value = Math.floor(a.value)
    b[i].value = Math.floor(b[i].value)
  }

  for (let i = 0; i < b.length; i += 1) {
    if ((a.num === 3 || a.num === 4) && (b[i].num === 1 || b[i].num === 2)) {
      a.value += b[i].value
      b[i].value *= lurate
    }
    if ((a.num === 3 || a.num === 4) && (b[i].num === 3 || b[i].num === 4)) {
      a.value = (a.value + b[i].value) * uurate
      b[i].value *= uurate
      // b[i].value *= lurate
    }
    if ((a.num === 3 || a.num === 4) && (b[i].num === 5 || b[i].num === 6)) {
      a.value -= (b[i].value / 2)
      b[i].value *= uurate
    }
    if ((a.num === 3 || a.num === 4) && (b[i].num === 7 || b[i].num === 8 || b[i].num === 9 || b[i].num === 10)) {
      a.value -= (b[i].value / 2)
      b[i].value *= lurate
    }
    a.value = Math.floor(a.value)
    b[i].value = Math.floor(b[i].value)
  }

  for (let i = 0; i < b.length; i += 1) {
    if ((a.num === 5 || a.num === 6) && (b[i].num === 3 || b[i].num === 4)) {
      a.value += b[i].value
      b[i].value *= lurate
    }
    if ((a.num === 5 || a.num === 6) && (b[i].num === 5 || b[i].num === 6)) {
      a.value = (a.value + b[i].value) * uurate
      b[i].value *= uurate
      // b[i].value *= lurate
    }
    if ((a.num === 5 || a.num === 6) && (b[i].num === 7 || b[i].num === 8)) {
      a.value -= (b[i].value / 2)
      b[i].value *= uurate
    }
    if ((a.num === 5 || a.num === 6) && (b[i].num === 9 || b[i].num === 10 || b[i].num === 1 || b[i].num === 2)) {
      a.value -= (b[i].value / 2)
      b[i].value *= lurate
    }
    a.value = Math.floor(a.value)
    b[i].value = Math.floor(b[i].value)
  }

  for (let i = 0; i < b.length; i += 1) {
    if ((a.num === 7 || a.num === 8) && (b[i].num === 5 || b[i].num === 6)) {
      a.value += b[i].value
      b[i].value *= lurate
    }
    if ((a.num === 7 || a.num === 8) && (b[i].num === 7 || b[i].num === 8)) {
      a.value = (a.value + b[i].value) * uurate
      b[i].value *= uurate
      // b[i].value *= lurate
    }
    if ((a.num === 7 || a.num === 8) && (b[i].num === 9 || b[i].num === 10)) {
      a.value -= (b[i].value / 2)
      b[i].value *= uurate
    }
    if ((a.num === 7 || a.num === 8) && (b[i].num === 1 || b[i].num === 2 || b[i].num === 3 || b[i].num === 4)) {
      a.value -= (b[i].value / 2)
      b[i].value *= lurate
    }
    a.value = Math.floor(a.value)
    b[i].value = Math.floor(b[i].value)
  }

  for (let i = 0; i < b.length; i += 1) {
    if ((a.num === 9 || a.num === 10) && (b[i].num === 7 || b[i].num === 8)) {
      a.value += b[i].value
      b[i].value *= lurate
    }
    if ((a.num === 9 || a.num === 10) && (b[i].num === 9 || b[i].num === 10)) {
      a.value = (a.value + b[i].value) * uurate
      b[i].value *= uurate
      // b[i].value *= lurate
    }
    if ((a.num === 9 || a.num === 10) && (b[i].num === 1 || b[i].num === 2)) {
      a.value -= (b[i].value / 2)
      b[i].value *= uurate
    }
    if ((a.num === 9 || a.num === 10) && (b[i].num === 3 || b[i].num === 4 || b[i].num === 5 || b[i].num === 6)) {
      a.value -= (b[i].value / 2)
      b[i].value *= lurate
    }
    a.value = Math.floor(a.value)
    b[i].value = Math.floor(b[i].value)
  }
  return [num1, num2]
}
export function calcWordValueB(num1, num2) {
  // 生系數
  // const urate = 1.5
  const uurate = 1.0
  const lurate = 0.7
  // 剋系數
  // const urate = 1.5
  // const uurate = 0.5
  // const lurate = 0.8
  const a = num1
  const b = num2
  for (let j = 0; j < a.length; j += 1) {
    for (let i = 0; i < b.length; i += 1) {
      if ((a[j].num === 1 || a[j].num === 2) && (b[i].num === 9 || b[i].num === 10)) {
        a[j].value += b[i].value
        b[i].value *= lurate
      }
      if ((a[j].num === 1 || a[j].num === 2) && (b[i].num === 1 || b[i].num === 2)) {
        a[j].value = (a[j].value + b[i].value) * uurate
        b[i].value *= uurate
        // b[i].value *= lurate
      }
      if ((a[j].num === 1 || a[j].num === 2) && (b[i].num === 3 || b[i].num === 4)) {
        a[j].value *= lurate
        b[i].value *= uurate
      }
      if ((a[j].num === 1 || a[j].num === 2) && (b[i].num === 5 || b[i].num === 6 || b[i].num === 7 || b[i].num === 8)) {
        a[j].value *= lurate
        b[i].value *= lurate
      }
      b[i].value = Math.floor(b[i].value)
    }
    a[j].value = Math.floor(a[j].value)
  }

  for (let j = 0; j < a.length; j += 1) {
    for (let i = 0; i < b.length; i += 1) {
      if ((a[j].num === 3 || a[j].num === 4) && (b[i].num === 1 || b[i].num === 2)) {
        a[j].value += b[i].value
        b[i].value *= lurate
      }
      if ((a[j].num === 3 || a[j].num === 4) && (b[i].num === 3 || b[i].num === 4)) {
        a[j].value = (a[j].value + b[i].value) * uurate
        b[i].value *= uurate
        // b[i].value *= lurate
      }
      if ((a[j].num === 3 || a[j].num === 4) && (b[i].num === 5 || b[i].num === 6)) {
        a[j].value *= lurate
        b[i].value *= uurate
      }
      if ((a[j].num === 3 || a[j].num === 4) && (b[i].num === 9 || b[i].num === 10 || b[i].num === 7 || b[i].num === 8)) {
        a[j].value *= lurate
        b[i].value *= lurate
      }
      b[i].value = Math.floor(b[i].value)
    }
    a[j].value = Math.floor(a[j].value)
  }

  for (let j = 0; j < a.length; j += 1) {
    for (let i = 0; i < b.length; i += 1) {
      if ((a[j].num === 5 || a[j].num === 6) && (b[i].num === 3 || b[i].num === 4)) {
        a[j].value += b[i].value
        b[i].value *= lurate
      }
      if ((a[j].num === 5 || a[j].num === 6) && (b[i].num === 5 || b[i].num === 6)) {
        a[j].value = (a[j].value + b[i].value) * uurate
        b[i].value *= uurate
        // b[i].value *= lurate
      }
      if ((a[j].num === 5 || a[j].num === 6) && (b[i].num === 7 || b[i].num === 8)) {
        a[j].value *= lurate
        b[i].value *= uurate
      }
      if ((a[j].num === 5 || a[j].num === 6) && (b[i].num === 1 || b[i].num === 2 || b[i].num === 9 || b[i].num === 10)) {
        a[j].value *= lurate
        b[i].value *= lurate
      }
      b[i].value = Math.floor(b[i].value)
    }
    a[j].value = Math.floor(a[j].value)
  }

  for (let j = 0; j < a.length; j += 1) {
    for (let i = 0; i < b.length; i += 1) {
      if ((a[j].num === 7 || a[j].num === 8) && (b[i].num === 5 || b[i].num === 6)) {
        a[j].value += b[i].value
        b[i].value *= lurate
      }
      if ((a[j].num === 7 || a[j].num === 8) && (b[i].num === 7 || b[i].num === 8)) {
        a[j].value = (a[j].value + b[i].value) * uurate
        b[i].value *= uurate
        // b[i].value *= lurate
      }
      if ((a[j].num === 7 || a[j].num === 8) && (b[i].num === 9 || b[i].num === 10)) {
        a[j].value *= lurate
        b[i].value *= uurate
      }
      if ((a[j].num === 7 || a[j].num === 8) && (b[i].num === 1 || b[i].num === 2 || b[i].num === 3 || b[i].num === 4)) {
        a[j].value *= lurate
        b[i].value *= lurate
      }
      b[i].value = Math.floor(b[i].value)
    }
    a[j].value = Math.floor(a[j].value)
  }

  for (let j = 0; j < a.length; j += 1) {
    for (let i = 0; i < b.length; i += 1) {
      if ((a[j].num === 9 || a[j].num === 10) && (b[i].num === 7 || b[i].num === 8)) {
        a[j].value += b[i].value
        b[i].value *= lurate
      }
      if ((a[j].num === 9 || a[j].num === 10) && (b[i].num === 9 || b[i].num === 10)) {
        a[j].value = (a[j].value + b[i].value) * uurate
        b[i].value *= uurate
        // b[i].value *= lurate
      }
      if ((a[j].num === 9 || a[j].num === 10) && (b[i].num === 1 || b[i].num === 2)) {
        a[j].value *= lurate
        b[i].value *= uurate
      }
      if ((a[j].num === 9 || a[j].num === 10) && (b[i].num === 5 || b[i].num === 6 || b[i].num === 3 || b[i].num === 4)) {
        a[j].value *= lurate
        b[i].value *= lurate
      }
      b[i].value = Math.floor(b[i].value)
    }
    a[j].value = Math.floor(a[j].value)
  }
  return [num1, num2]
}
