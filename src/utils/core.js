import { differenceInDays } from 'date-fns'
import _ from 'lodash'

function getDayTownName(num) {
  switch (num) {
    case 1: return '甲子'
    case 2: return '乙丑'
    case 3: return '丙寅'
    case 4: return '丁卯'
    case 5: return '戊辰'
    case 6: return '己巳'
    case 7: return '庚午'
    case 8: return '辛未'
    case 9: return '壬申'
    case 10: return '癸酉'
    case 11: return '甲戌'
    case 12: return '乙亥'
    case 13: return '丙子'
    case 14: return '丁丑'
    case 15: return '戊寅'
    case 16: return '己卯'
    case 17: return '庚辰'
    case 18: return '辛巳'
    case 19: return '壬午'
    case 20: return '癸未'
    case 21: return '甲申'
    case 22: return '乙酉'
    case 23: return '丙戌'
    case 24: return '丁亥'
    case 25: return '戊子'
    case 26: return '己丑'
    case 27: return '庚寅'
    case 28: return '辛卯'
    case 29: return '壬辰'
    case 30: return '癸巳'
    case 31: return '甲午'
    case 32: return '乙未'
    case 33: return '丙申'
    case 34: return '丁酉'
    case 35: return '戊戌'
    case 36: return '己亥'
    case 37: return '庚子'
    case 38: return '辛丑'
    case 39: return '壬寅'
    case 40: return '癸卯'
    case 41: return '甲辰'
    case 42: return '乙巳'
    case 43: return '丙午'
    case 44: return '丁未'
    case 45: return '戊申'
    case 46: return '己酉'
    case 47: return '庚戌'
    case 48: return '辛亥'
    case 49: return '壬子'
    case 50: return '癸丑'
    case 51: return '甲寅'
    case 52: return '乙卯'
    case 53: return '丙辰'
    case 54: return '丁巳'
    case 55: return '戊午'
    case 56: return '己未'
    case 57: return '庚申'
    case 58: return '辛酉'
    case 59: return '壬戌'
    case 60: return '癸亥'
    case 0: return '癸亥'
    default: return ''
  }
}

function getDayTownArr(num) {
  switch (num) {
    case 1: return [0 + 1, 0 + 1]
    case 2: return [1 + 1, 1 + 1]
    case 3: return [2 + 1, 2 + 1]
    case 4: return [3 + 1, 3 + 1]
    case 5: return [4 + 1, 4 + 1]
    case 6: return [5 + 1, 5 + 1]
    case 7: return [6 + 1, 6 + 1]
    case 8: return [7 + 1, 7 + 1]
    case 9: return [8 + 1, 8 + 1]
    case 10: return [9 + 1, 9 + 1]
    case 11: return [0 + 1, 10 + 1]
    case 12: return [1 + 1, 11 + 1]
    case 13: return [2 + 1, 0 + 1]
    case 14: return [3 + 1, 1 + 1]
    case 15: return [4 + 1, 2 + 1]
    case 16: return [5 + 1, 3 + 1]
    case 17: return [6 + 1, 4 + 1]
    case 18: return [7 + 1, 5 + 1]
    case 19: return [8 + 1, 6 + 1]
    case 20: return [9 + 1, 7 + 1]
    case 21: return [0 + 1, 8 + 1]
    case 22: return [1 + 1, 9 + 1]
    case 23: return [2 + 1, 10 + 1]
    case 24: return [3 + 1, 11 + 1]
    case 25: return [4 + 1, 0 + 1]
    case 26: return [5 + 1, 1 + 1]
    case 27: return [6 + 1, 2 + 1]
    case 28: return [7 + 1, 3 + 1]
    case 29: return [8 + 1, 4 + 1]
    case 30: return [9 + 1, 5 + 1]
    case 31: return [0 + 1, 6 + 1]
    case 32: return [1 + 1, 7 + 1]
    case 33: return [2 + 1, 8 + 1]
    case 34: return [3 + 1, 9 + 1]
    case 35: return [4 + 1, 10 + 1]
    case 36: return [5 + 1, 11 + 1]
    case 37: return [6 + 1, 0 + 1]
    case 38: return [7 + 1, 1 + 1]
    case 39: return [8 + 1, 2 + 1]
    case 40: return [9 + 1, 3 + 1]
    case 41: return [0 + 1, 4 + 1]
    case 42: return [1 + 1, 5 + 1]
    case 43: return [2 + 1, 6 + 1]
    case 44: return [3 + 1, 7 + 1]
    case 45: return [4 + 1, 8 + 1]
    case 46: return [5 + 1, 9 + 1]
    case 47: return [6 + 1, 10 + 1]
    case 48: return [7 + 1, 11 + 1]
    case 49: return [8 + 1, 0 + 1]
    case 50: return [9 + 1, 1 + 1]
    case 51: return [0 + 1, 2 + 1]
    case 52: return [1 + 1, 3 + 1]
    case 53: return [2 + 1, 4 + 1]
    case 54: return [3 + 1, 5 + 1]
    case 55: return [4 + 1, 6 + 1]
    case 56: return [5 + 1, 7 + 1]
    case 57: return [6 + 1, 8 + 1]
    case 58: return [7 + 1, 9 + 1]
    case 59: return [8 + 1, 10 + 1]
    case 60: return [9 + 1, 11 + 1]
    case 0: return [9 + 1, 11 + 1]
    default: return [0 + 1, 0 + 1]
  }
}

export function convertToLocalTime(yr, mon, day, min, sec) {
  // console.log(yr, mon, day, min, sec)
  const newdate = {
    year: yr,
    mon: mon.toString().length > 1 ? mon : `0${mon.toString()}`,
    day: day.toString().length > 1 ? day : `0${day.toString()}`,
    min: min.toString().length > 1 ? min : `0${min.toString()}`,
    sec: sec.toString().length > 1 ? sec : `0${sec.toString()}`,
  }

  const newdateStr = `${newdate.year}-${newdate.mon}-${newdate.day}T${newdate.min}:${newdate.sec}:00Z`
  const aa = new Date(newdateStr)
  const ddd = new Date().getTimezoneOffset()
  const b = aa.getTime() + (ddd * 60000)
  const dsa = new Date(b)
  return dsa
}


/**
 * diffNum：相差日
 * optNum：100年差值
 * curdate：輸入日期
 * r: 日柱Array
 * @param {Number} year
 * @param {Number} mon
 * @param {Number} date
 */
function getDayTown(year, mon, date, mode = 2) {
  // const date1 = new Date('1990-10-1')
  // const date1 = new Date('1980-2-21')
  // const date1 = new Date('1920-3-6')
  const date1 = convertToLocalTime(1920, 3, 6, 0, 0)
  // const date1 = new Date('1971-2-8')
  // const date1 = new Date('1900-2-19')
  // const dataStr = `${year}-${mon}-${date}`
  // debugger
  const curdate = convertToLocalTime(year, mon, date, 0, 0)
  // const curdate = new Date(dataStr)

  const result = differenceInDays(
    curdate,
    date1,
  )
  // debugger
  const r = (result % 60)
  switch (mode) {
    case 1: return getDayTownName(r)
    case 2: return getDayTownArr(r)
    default: return r
  }
}

export function getSpringStartDay(yr) {
  // 1904 year before cant calc
  const a = convertToLocalTime(2019, 2, 4, 11, 14)
  const b = convertToLocalTime(yr, 1, 1, 0, 0)
  const c = a.getYear() - b.getYear()
  const between2yrNum = 31556940000
  if (c >= 0) {
    return new Date(a.getTime() - (between2yrNum * c))
  }
  return new Date(a.getTime() + (between2yrNum * Math.abs(c)))
}

export function getUserUseSpringDay(yr, mon, day, hr, min) {
  const tmpYrSpringDay = getSpringStartDay(yr)
  const tmpB4YrSpringDay = getSpringStartDay(parseInt(yr, 10) - 1)
  const curBDate = convertToLocalTime(yr, mon, day, hr, min)
  const diff = curBDate - tmpYrSpringDay
  if (diff >= 0) {
    return tmpYrSpringDay
  }
  return tmpB4YrSpringDay
}

export function getYearrTop(yr) {
  const index = Number(yr.toString().slice(-1, yr.length))
  switch (index) {
    case 4: return 1
    case 5: return 2
    case 6: return 3
    case 7: return 4
    case 8: return 5
    case 9: return 6
    case 0: return 7
    case 1: return 8
    case 2: return 9
    case 3: return 10
    default: return 0
  }
}

export function getYearBottom(yr) {
  switch ((Number(yr)) % 12) {
    case 4: return 1
    case 5: return 2
    case 6: return 3
    case 7: return 4
    case 8: return 5
    case 9: return 6
    case 10: return 7
    case 11: return 8
    case 0: return 9
    case 1: return 10
    case 2: return 11
    case 3: return 12
    default: return 0
  }
}

export function getUserYearTown(yr, mon, day, hr, min) {
  const tmpYrSpringDay = getSpringStartDay(yr)
  const curBDate = convertToLocalTime(yr, mon, day, hr, min)
  const diff = curBDate - tmpYrSpringDay
  if (diff >= 0) {
    return [getYearrTop(parseInt(yr, 10)),
      getYearBottom(parseInt(yr, 10))]
  }
  return [getYearrTop(parseInt(yr, 10) - 1),
    getYearBottom(parseInt(yr - 1, 10))]
}

export function get24EventDateList(firstDate) {
  const list = []
  const tzone = [1281000000,
    1289160000,
    1298880000,
    1309980000,
    1321440000,
    1332480000,
    1342560000,
    1350420000,
    1356480000,
    1358820000,
    1358940000,
    1354980000,
    1349340000,
    1340100000,
    1330380000,
    1318560000,
    1307640000,
    1296240000,
    1287300000,
    1279140000,
    1274460000,
    1271460000,
    1272300000,
    1274880000]
  let tmpNum = 0
  // console.log(tzone)
  list.push(firstDate)
  for (let i = 0; i < 24; i += 1) {
    tmpNum += tzone[i]
    const aaa = new Date(firstDate.getTime() + tmpNum)
    list.push(aaa)
  }
  return list
}

function getMonTop(num) {
  switch (num) {
    case 6: return [3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4]
    case 1: return [3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4]
    case 7: return [5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6]
    case 2: return [5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6]
    case 8: return [7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8]
    case 3: return [7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8]
    case 9: return [9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10]
    case 4: return [9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10]
    case 10: return [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2]
    case 5: return [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2]
    default: return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  }
}

export function getUserMonthTown(date, list) {
  let selMonTop = 0
  let selMonBottom = 0
  const listYr = new Date(list[0]).getFullYear()
  const YrTop = getYearrTop(listYr)
  const monList = getMonTop(YrTop)
  for (let i = 0; i < list.length; i += 1) {
    const tgdate = new Date(list[i])
    if (date < tgdate) {
      selMonTop = monList[i - 1]
      selMonBottom = i
      break
    }
  }
  let lastbottom = 0
  switch (selMonBottom) {
    case 21: lastbottom = 1; break
    case 22: lastbottom = 1; break
    case 23: lastbottom = 2; break
    case 24: lastbottom = 2; break
    case 1: lastbottom = 3; break
    case 2: lastbottom = 3; break
    case 3: lastbottom = 4; break
    case 4: lastbottom = 4; break
    case 5: lastbottom = 5; break
    case 6: lastbottom = 5; break
    case 7: lastbottom = 6; break
    case 8: lastbottom = 6; break
    case 9: lastbottom = 7; break
    case 10: lastbottom = 7; break
    case 11: lastbottom = 8; break
    case 12: lastbottom = 8; break
    case 13: lastbottom = 9; break
    case 14: lastbottom = 9; break
    case 15: lastbottom = 10; break
    case 16: lastbottom = 10; break
    case 17: lastbottom = 11; break
    case 18: lastbottom = 11; break
    case 19: lastbottom = 12; break
    case 20: lastbottom = 12; break
    default: lastbottom = 0
  }
  // debugger

  return [selMonTop, lastbottom]
}

export function checkReseve(gender, yrTop) {
  const isOdd = yrTop % 2 === 0

  if ((gender && !isOdd) || (!gender && isOdd)) {
    // console.log('go')
    return false
  }
  // console.log('back')
  return true
}

export function getBigChannel(gender, yrTop, monArray) {
  let topNum = monArray[0]
  let bottomNum = monArray[1]
  const totalChannelNum = 10
  const topMinNum = 1
  const topMaxNum = 10
  const bottomMinNum = 1
  const bottomMaxNum = 12
  const channellist = []
  const reserve = checkReseve(gender, yrTop)
  for (let i = 0; i < totalChannelNum; i += 1) {
    if (!reserve) {
      // 順
      if (topNum === topMaxNum) {
        topNum = topMinNum
      } else {
        topNum += 1
      }

      if (bottomNum === bottomMaxNum) {
        bottomNum = bottomMinNum
      } else {
        bottomNum += 1
      }

      channellist.push([topNum, bottomNum])
    } else {
      // 反
      if (topNum === topMinNum) {
        topNum = topMaxNum
      } else {
        topNum -= 1
      }

      if (bottomNum === bottomMinNum) {
        bottomNum = bottomMaxNum
      } else {
        bottomNum -= 1
      }

      channellist.push([topNum, bottomNum])
    }
  }
  return channellist
}

export function getTimeTop(num) {
  switch (num - 1) {
    case 0: return [1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3]
    case 5: return [1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3]
    case 1: return [3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5]
    case 6: return [3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5]
    case 2: return [5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7]
    case 7: return [5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7]
    case 3: return [7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9]
    case 8: return [7, 8, 8, 9, 9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9]
    case 4: return [9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1]
    case 9: return [9, 10, 10, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 1]
    default: return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  }
}

export function getTimeBottom(num) {
  switch (num) {
    case 23: return 1
    case 0: return 1
    case 1: return 2
    case 2: return 2
    case 3: return 3
    case 4: return 3
    case 5: return 4
    case 6: return 4
    case 7: return 5
    case 8: return 5
    case 9: return 6
    case 10: return 6
    case 11: return 7
    case 12: return 7
    case 13: return 8
    case 14: return 8
    case 15: return 9
    case 16: return 9
    case 17: return 10
    case 18: return 10
    case 19: return 11
    case 20: return 11
    case 21: return 12
    case 22: return 12
    default: return 0
  }
}

export function getUserTimeTown(dayTopNum, date) {
  // console.log(date.getHours())
  // console.log(getTimeTop(dayTopNum[0]))
  return [getTimeTop(dayTopNum[0])[date.getHours()], getTimeBottom(date.getHours())]
}

export function getWordPowerT(num) {
  switch (num) {
    case 1: return [{ num: 1, value: 100 }]
    case 2: return [{ num: 2, value: 100 }]
    case 3: return [{ num: 3, value: 100 }]
    case 4: return [{ num: 4, value: 100 }]
    case 5: return [{ num: 5, value: 100 }]
    case 6: return [{ num: 6, value: 100 }]
    case 7: return [{ num: 7, value: 100 }]
    case 8: return [{ num: 8, value: 100 }]
    case 9: return [{ num: 9, value: 100 }]
    case 10: return [{ num: 10, value: 100 }]
    default: return [{ num: 0, value: 100 }]
  }
}

export function getWordPowerB(num) {
  const oldFlag = false
  if (oldFlag) {
    switch (num) {
      case 1: return [{ num: 10, value: 100 }]
      case 2: return [{ num: 6, value: 100 }]
      case 3: return [{ num: 1, value: 100 }]
      case 4: return [{ num: 2, value: 100 }]
      case 5: return [{ num: 5, value: 100 }]
      case 6: return [{ num: 3, value: 100 }]
      case 7: return [{ num: 4, value: 100 }]
      case 8: return [{ num: 6, value: 100 }]
      case 9: return [{ num: 7, value: 100 }]
      case 10: return [{ num: 8, value: 100 }]
      case 11: return [{ num: 5, value: 100 }]
      case 12: return [{ num: 9, value: 100 }]
      default: return [{ num: 0, value: 100 }]
    }
  } else {
    switch (num) {
      case 1: return [{ num: 10, value: 67 }, { num: 9, value: 33 }]
      case 2: return [{ num: 6, value: 54 }, { num: 10, value: 33 }, { num: 8, value: 13 }]
      case 3: return [{ num: 1, value: 46 }, { num: 3, value: 27 }, { num: 5, value: 27 }]
      case 4: return [{ num: 2, value: 67 }, { num: 1, value: 33 }]
      case 5: return [{ num: 5, value: 57 }, { num: 2, value: 33 }, { num: 10, value: 10 }]
      case 6: return [{ num: 3, value: 50 }, { num: 5, value: 33 }, { num: 7, value: 17 }]
      case 7: return [{ num: 4, value: 34 }, { num: 3, value: 33 }, { num: 6, value: 33 }]
      case 8: return [{ num: 6, value: 63 }, { num: 4, value: 24 }, { num: 2, value: 13 }]
      case 9: return [{ num: 7, value: 63 }, { num: 9, value: 24 }, { num: 5, value: 13 }]
      case 10: return [{ num: 8, value: 67 }, { num: 7, value: 33 }]
      case 11: return [{ num: 5, value: 60 }, { num: 8, value: 33 }, { num: 4, value: 7 }]
      case 12: return [{ num: 9, value: 67 }, { num: 1, value: 33 }]
      default: return [{ num: 0, value: 100 }]
    }
  }
}

export function getWordPowerB2(num) {
  const oldFlag = false
  if (oldFlag) {
    switch (num) {
      case 1: return [{ num: 10, value: 100 }]
      case 2: return [{ num: 6, value: 100 }]
      case 3: return [{ num: 1, value: 100 }]
      case 4: return [{ num: 2, value: 100 }]
      case 5: return [{ num: 5, value: 100 }]
      case 6: return [{ num: 3, value: 100 }]
      case 7: return [{ num: 4, value: 100 }]
      case 8: return [{ num: 6, value: 100 }]
      case 9: return [{ num: 7, value: 100 }]
      case 10: return [{ num: 8, value: 100 }]
      case 11: return [{ num: 5, value: 100 }]
      case 12: return [{ num: 9, value: 100 }]
      default: return [{ num: 0, value: 100 }]
    }
  } else {
    switch (num) {
      case 1: return [{ num: 10, value: 67 }, { num: 9, value: 33 }]
      case 2: return [{ num: 6, value: 54 }, { num: 10, value: 33 }, { num: 8, value: 13 }]
      case 3: return [{ num: 1, value: 46 }, { num: 3, value: 27 }, { num: 5, value: 27 }]
      case 4: return [{ num: 2, value: 67 }, { num: 1, value: 33 }]
      case 5: return [{ num: 5, value: 57 }, { num: 2, value: 33 }, { num: 10, value: 10 }]
      case 6: return [{ num: 3, value: 50 }, { num: 5, value: 33 }, { num: 7, value: 17 }]
      case 7: return [{ num: 4, value: 34 }, { num: 3, value: 33 }, { num: 6, value: 33 }]
      case 8: return [{ num: 6, value: 63 }, { num: 4, value: 24 }, { num: 2, value: 13 }]
      case 9: return [{ num: 7, value: 63 }, { num: 9, value: 24 }, { num: 5, value: 13 }]
      case 10: return [{ num: 8, value: 67 }, { num: 7, value: 33 }]
      case 11: return [{ num: 5, value: 60 }, { num: 8, value: 33 }, { num: 4, value: 7 }]
      case 12: return [{ num: 9, value: 67 }, { num: 1, value: 33 }]
      default: return [{ num: 0, value: 100 }]
    }
  }
}

export function isRunnian(yr) {
  const year = Number(yr)
  if (year % 100 === 0) {
    return year % 400 === 0
  }
  return year % 4 === 0
}

export function convToNextDay(year, mon, date, hour, min, mode) {
  let cyear = year
  let cmon = mon
  let cdate = date
  let chour = hour
  let cmin = min
  if (hour === 23 && mode) {
    chour = 0
    cmin = 0
    switch (mon) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        if (date === 31) {
          cdate = 1
          if (mon === 12) {
            cyear += 1
            cmon = 1
          } else {
            cmon += 1
          }
        } else {
          cdate += 1
        }
        break
      case 4:
      case 6:
      case 9:
      case 11:
        if (date === 30) {
          cdate = 1
          cmon += 1
        } else {
          cdate += 1
        }
        break
      case 2:
      default:
        if (isRunnian(year)) {
          if (date === 29) {
            cdate = 1
            cmon += 1
          } else {
            cdate += 1
          }
        } else if (date === 28) {
          cdate = 1
          cmon += 1
        } else {
          cdate += 1
        }
    }
  }
  return [cyear, cmon, cdate, chour, cmin]
}

export function getFourTown(year, mon, date, hour, min, mode) {
  const cData = {
    year: convToNextDay(year, mon, date, hour, min, mode)[0],
    mon: convToNextDay(year, mon, date, hour, min, mode)[1],
    date: convToNextDay(year, mon, date, hour, min, mode)[2],
    hour: convToNextDay(year, mon, date, hour, min, mode)[3],
    min: convToNextDay(year, mon, date, hour, min, mode)[4],
  }

  return [
    getUserYearTown(cData.year, cData.mon, cData.date, cData.hour, cData.min),
    getUserMonthTown(convertToLocalTime(cData.year, cData.mon, cData.date, cData.hour, cData.min),
      get24EventDateList(getUserUseSpringDay(cData.year, cData.mon, cData.date, cData.hour, cData.min))),
    getDayTown(cData.year, cData.mon, cData.date),
    getUserTimeTown(getDayTown(cData.year, cData.mon, cData.date, 2),
      convertToLocalTime(cData.year, cData.mon, cData.date, cData.hour, cData.min)),
  ]
}

export function getBigBig(year, mon, date, hour, min, mode, sex) {
  const cData = {
    year: convToNextDay(year, mon, date, hour, min, mode)[0],
    mon: convToNextDay(year, mon, date, hour, min, mode)[1],
    date: convToNextDay(year, mon, date, hour, min, mode)[2],
    hour: convToNextDay(year, mon, date, hour, min, mode)[3],
    min: convToNextDay(year, mon, date, hour, min, mode)[4],
  }

  const a = getBigChannel(
    sex,
    getUserYearTown(cData.year, cData.mon, cData.date, cData.hour, cData.min)[0],
    getUserMonthTown(convertToLocalTime(cData.year, cData.mon, cData.date, cData.hour, cData.min),
      get24EventDateList(getUserUseSpringDay(cData.year, cData.mon, cData.date, cData.hour, cData.min))),
  )
  return a
}

export function getstartchannelDay(reserve, curBDate, channelArray) {
  const newArray = []
  let lastDay = null
  for (let j = 0; j < channelArray.length; j += 1) {
    if (j === 0 || j % 2 === 0) {
      newArray.push(channelArray[j])
    }
  }
  for (let i = 0; i < newArray.length; i += 1) {
    if (!reserve) {
      if (curBDate < new Date(newArray[i])) {
        const hours = Math.abs(curBDate - newArray[i]) / 36e5
        // conv to china time then * 10 to date and add curbdate
        lastDay = new Date(curBDate.getTime() + (hours / 2 * 10 * 24 * 60 * 60 * 1000))
        break
      }
    } else if (curBDate < new Date(newArray[i])) {
      const hours = Math.abs(curBDate - newArray[i - 1]) / 36e5
      // conv to china time then * 10 to date and add curbdate
      lastDay = new Date(curBDate.getTime() + (hours / 2 * 10 * 24 * 60 * 60 * 1000))
      break
    }
  }
  return lastDay
}

export function getChannelsTenYrDate(curStartDate) {
  const newArray = []
  for (let i = 0; i < 20; i += 1) {
    newArray.push(new Date(curStartDate.getTime() + 31556940000 * (i * 5)))
  }

  const wa = _.chunk(newArray, 2)

  return wa
}

export function getDynamicBigChannelTown(year, mon, date, hour, min, mode, sex, dyyear) {
  const cData = {
    year: convToNextDay(year, mon, date, hour, min, mode)[0],
    mon: convToNextDay(year, mon, date, hour, min, mode)[1],
    date: convToNextDay(year, mon, date, hour, min, mode)[2],
    hour: convToNextDay(year, mon, date, hour, min, mode)[3],
    min: convToNextDay(year, mon, date, hour, min, mode)[4],
  }

  const a1 = checkReseve(sex, getUserYearTown(cData.year, cData.mon, cData.date, cData.hour, cData.min)[0])
  const b1 = convertToLocalTime(cData.year, cData.mon, cData.date, cData.hour, cData.min)
  const c1 = getUserUseSpringDay(Number(cData.year), Number(cData.mon), Number(cData.date), cData.hour, cData.min)
  const c2 = get24EventDateList(c1)
  const a = getstartchannelDay(
    a1,
    b1,
    c2,
  )
  const b = getChannelsTenYrDate(a)

  const xccc = convertToLocalTime(dyyear, a.getMonth() + 1, a.getDate(), 0, 0)
  // debugger

  // let c = new Date()
  const c = xccc
  let twon = 0
  for (let i = 0; i < b.length; i += 1) {
    if (c < b[i][0]) {
      if (i !== 0) {
        twon = i - 1
      }
      break
    } else {
      twon = 9
    }
  }
  const channel = getBigBig(year, mon, date, hour, min, mode, sex)

  return [channel[twon][0], channel[twon][1]]
}

export function getBigChannelTown(year, mon, date, hour, min, mode, sex) {
  const cData = {
    year: convToNextDay(year, mon, date, hour, min, mode)[0],
    mon: convToNextDay(year, mon, date, hour, min, mode)[1],
    date: convToNextDay(year, mon, date, hour, min, mode)[2],
    hour: convToNextDay(year, mon, date, hour, min, mode)[3],
    min: convToNextDay(year, mon, date, hour, min, mode)[4],
  }
  const a1 = checkReseve(sex, getUserYearTown(cData.year, cData.mon, cData.date, cData.hour, cData.min)[0])
  const b1 = convertToLocalTime(cData.year, cData.mon, cData.date, cData.hour, cData.min)
  const c1 = getUserUseSpringDay(Number(cData.year), Number(cData.mon), Number(cData.date), cData.hour, cData.min)
  const c2 = get24EventDateList(c1)
  const a = getstartchannelDay(
    a1,
    b1,
    c2,
  )
  const b = getChannelsTenYrDate(a)
  return b
}

export function calctopbottom(t1, t2) {
  if ((t1 === 1 && t2 === 8)
    || (t1 === 8 && t2 === 1)
    || (t1 === 2 && t2 === 7)
    || (t1 === 7 && t2 === 2)
    || (t1 === 3 && t2 === 6)
    || (t1 === 6 && t2 === 3)
    || (t1 === 4 && t2 === 5)
    || (t1 === 5 && t2 === 4)
    || (t1 === 12 && t2 === 9)
    || (t1 === 9 && t2 === 12)
    || (t1 === 11 && t2 === 10)
    || (t1 === 10 && t2 === 11)) {
    return true
  }
  return false
}
export function calc180b(t1, t2) {
  if ((t1 === 1 && t2 === 7)
    || (t1 === 7 && t2 === 1)
    || (t1 === 2 && t2 === 8)
    || (t1 === 8 && t2 === 2)
    || (t1 === 3 && t2 === 9)
    || (t1 === 9 && t2 === 3)
    || (t1 === 4 && t2 === 10)
    || (t1 === 10 && t2 === 4)
    || (t1 === 5 && t2 === 11)
    || (t1 === 11 && t2 === 5)
    || (t1 === 6 && t2 === 12)
    || (t1 === 12 && t2 === 6)) {
    return true
  }
  return false
}

export function calc150b(t1, t2) {
  if ((t1 === 1 && (t2 === 6 || t2 === 8))
    || (t1 === 2 && (t2 === 7 || t2 === 9))
    || (t1 === 3 && (t2 === 8 || t2 === 10))
    || (t1 === 4 && (t2 === 9 || t2 === 11))
    || (t1 === 5 && (t2 === 10 || t2 === 12))
    || (t1 === 6 && (t2 === 11 || t2 === 1))
    || (t1 === 7 && (t2 === 12 || t2 === 2))
    || (t1 === 8 && (t2 === 1 || t2 === 3))
    || (t1 === 9 && (t2 === 2 || t2 === 4))
    || (t1 === 10 && (t2 === 3 || t2 === 5))
    || (t1 === 11 && (t2 === 4 || t2 === 6))
    || (t1 === 12 && (t2 === 5 || t2 === 7))) {
    return true
  }
  return false
}

export function calc60cb(t1, t2) {
  if ((t1 === 1 && (t2 === 3 || t2 === 11))
    || (t1 === 2 && (t2 === 4 || t2 === 12))
    || (t1 === 3 && (t2 === 5 || t2 === 1))
    || (t1 === 4 && (t2 === 6 || t2 === 2))
    || (t1 === 5 && (t2 === 7 || t2 === 3))
    || (t1 === 6 && (t2 === 8 || t2 === 4))
    || (t1 === 7 && (t2 === 9 || t2 === 5))
    || (t1 === 8 && (t2 === 10 || t2 === 6))
    || (t1 === 9 && (t2 === 11 || t2 === 7))
    || (t1 === 10 && (t2 === 12 || t2 === 8))
    || (t1 === 11 && (t2 === 1 || t2 === 9))
    || (t1 === 12 && (t2 === 2 || t2 === 10))) {
    return true
  }
  return false
}

export function calc90b(t1, t2) {
  if ((t1 === 3 && (t2 === 6 || t2 === 12))
    || (t1 === 6 && (t2 === 3 || t2 === 9))
    || (t1 === 9 && (t2 === 6 || t2 === 12))
    || (t1 === 12 && (t2 === 3 || t2 === 9))
    || (t1 === 2 && (t2 === 5 || t2 === 11))
    || (t1 === 5 && (t2 === 2 || t2 === 8))
    || (t1 === 8 && (t2 === 5 || t2 === 11))
    || (t1 === 11 && (t2 === 2 || t2 === 8))
    || (t1 === 1 && (t2 === 4 || t2 === 10))
    || (t1 === 4 && (t2 === 1 || t2 === 7))
    || (t1 === 7 && (t2 === 4 || t2 === 10))
    || (t1 === 10 && (t2 === 1 || t2 === 7))) {
    return true
  }
  return false
}

export function calcn60b(t1, t2) {
  if ((t1 === 1 && (t2 === 3 || t2 === 11))
    || (t1 === 2 && (t2 === 4 || t2 === 12))
    || (t1 === 3 && (t2 === 5 || t2 === 1))
    || (t1 === 4 && (t2 === 6 || t2 === 2))
    || (t1 === 5 && (t2 === 7 || t2 === 3))
    || (t1 === 6 && (t2 === 8 || t2 === 4))
    || (t1 === 7 && (t2 === 9 || t2 === 5))
    || (t1 === 8 && (t2 === 10 || t2 === 6))
    || (t1 === 9 && (t2 === 11 || t2 === 7))
    || (t1 === 10 && (t2 === 12 || t2 === 8))
    || (t1 === 11 && (t2 === 1 || t2 === 9))
    || (t1 === 12 && (t2 === 2 || t2 === 10))) {
    return true
  }
  return false
}

export function calc60b(t1, t2) {
  if (((t1 === 1 && t2 === 2) || (t1 === 2 && t2 === 1))
    || ((t1 === 3 && t2 === 12) || (t1 === 12 && t2 === 3))
    || ((t1 === 4 && t2 === 11) || (t1 === 11 && t2 === 4))
    || ((t1 === 5 && t2 === 10) || (t1 === 10 && t2 === 5))
    || ((t1 === 6 && t2 === 9) || (t1 === 9 && t2 === 6))
    || ((t1 === 7 && t2 === 8) || (t1 === 8 && t2 === 7))) {
    return true
  }
  return false
}

export function calc0b(t1, t2) {
  if ((t1 === 1 && (t2 === 2 || t2 === 12))
    || (t1 === 2 && (t2 === 1 || t2 === 12))
    || (t1 === 12 && (t2 === 2 || t2 === 1))
    || (t1 === 3 && (t2 === 4 || t2 === 5))
    || (t1 === 4 && (t2 === 3 || t2 === 5))
    || (t1 === 5 && (t2 === 3 || t2 === 4))
    || (t1 === 6 && (t2 === 7 || t2 === 8))
    || (t1 === 7 && (t2 === 8 || t2 === 6))
    || (t1 === 8 && (t2 === 6 || t2 === 7))
    || (t1 === 9 && (t2 === 10 || t2 === 11))
    || (t1 === 10 && (t2 === 11 || t2 === 9))
    || (t1 === 11 && (t2 === 9 || t2 === 10))) {
    return true
  }
  return false
}

export function calc120b(t1, t2) {
  if ((t1 === 1 && (t2 === 5 || t2 === 9))
    || (t1 === 5 && (t2 === 1 || t2 === 9))
    || (t1 === 9 && (t2 === 5 || t2 === 1))
    || (t1 === 3 && (t2 === 7 || t2 === 11))
    || (t1 === 7 && (t2 === 3 || t2 === 11))
    || (t1 === 11 && (t2 === 7 || t2 === 3))
    || (t1 === 2 && (t2 === 6 || t2 === 10))
    || (t1 === 6 && (t2 === 2 || t2 === 10))
    || (t1 === 10 && (t2 === 6 || t2 === 2))
    || (t1 === 4 && (t2 === 8 || t2 === 12))
    || (t1 === 8 && (t2 === 4 || t2 === 12))
    || (t1 === 12 && (t2 === 8 || t2 === 4))) {
    return true
  }
  return false
}

export function calcTMerge(t1, t2) {
  if ((t1 === 1 && t2 === 6) || (t2 === 1 && t1 === 6)) {
    return true
  }
  if ((t1 === 2 && t2 === 7) || (t2 === 2 && t1 === 7)) {
    return true
  }
  if ((t1 === 3 && t2 === 8) || (t2 === 3 && t1 === 8)) {
    return true
  }
  if ((t1 === 4 && t2 === 9) || (t2 === 4 && t1 === 9)) {
    return true
  }
  if ((t1 === 5 && t2 === 10) || (t2 === 5 && t1 === 10)) {
    return true
  }
  return false
}

export function calc5Merge(t1, b1, day) {
  const last = [t1, b1]
  if (t1 === 1 && !day && (b1 === 7 || b1 === 8 || b1 === 11)) {
    last[0] = 5
  }
  if (t1 === 6 && !day && (b1 === 8 || b1 === 10 || b1 === 12)) {
    last[0] = 6
  }
  if (t1 === 2 && !day && (b1 === 2 || b1 === 5 || b1 === 10)) {
    last[0] = 8
  }
  if (t1 === 7 && !day && (b1 === 5 || b1 === 9 || b1 === 7)) {
    last[0] = 7
  }
  if (t1 === 3 && !day && (b1 === 1 || b1 === 5 || b1 === 9)) {
    last[0] = 9
  }
  if (t1 === 8 && !day && (b1 === 12)) {
    last[0] = 10
  }
  if (t1 === 4 && !day && (b1 === 4 || b1 === 12)) {
    last[0] = 2
  }
  if (t1 === 9 && !day && (b1 === 3 || b1 === 5)) {
    last[0] = 1
  }
  if (t1 === 5 && !day && (b1 === 3 || b1 === 7)) {
    last[0] = 3
  }
  if (t1 === 10 && !day && (b1 === 4 || b1 === 6)) {
    last[0] = 4
  }
  return last
}

export function calc3MType(t) {
  if (t === 1 || t === 5 || t === 9) {
    return 1
  }
  if (t === 3 || t === 7 || t === 11) {
    return 2
  }
  if (t === 2 || t === 6 || t === 10) {
    return 4
  }
  if (t === 4 || t === 8 || t === 12) {
    return 5
  }
  return 0
}

export function calc3GType(t) {
  if (t === 1 || t === 2 || t === 12) {
    return 5
    // return 1
  }
  if (t === 3 || t === 4 || t === 5) {
    return 1
    // return 2
  }
  if (t === 6 || t === 7 || t === 8) {
    return 2
    // return 4
  }
  if (t === 9 || t === 10 || t === 11) {
    return 4
    // return 5
  }
  return 0
}

export function calc6MType(t) {
  if (t === 1 || t === 2) {
    return 3
  }
  if (t === 3 || t === 12) {
    return 1
  }
  if (t === 4 || t === 11) {
    return 2
  }
  if (t === 5 || t === 10) {
    return 4
  }
  if (t === 6 || t === 9) {
    return 5
  }
  if (t === 7 || t === 8) {
    return 2
  }
  return 0
}

export function calc6M(t1, t2) {
  if ((t1 === 1 && t2 === 2)
    || (t1 === 2 && t2 === 1)
    || (t1 === 3 && t2 === 12)
    || (t1 === 12 && t2 === 3)
    || (t1 === 4 && t2 === 11)
    || (t1 === 11 && t2 === 4)
    || (t1 === 5 && t2 === 10)
    || (t1 === 10 && t2 === 5)
    || (t1 === 6 && t2 === 9)
    || (t1 === 9 && t2 === 6)
    || (t1 === 7 && t2 === 8)
    || (t1 === 8 && t2 === 7)) {
    return true
  }
  return false
}

export function calc180BType(t1, t2) {
  if ((t1 === 1 && t2 === 7)
    || (t1 === 7 && t2 === 1)
    || (t1 === 2 && t2 === 8)
    || (t1 === 8 && t2 === 2)
    || (t1 === 3 && t2 === 9)
    || (t1 === 9 && t2 === 3)
    || (t1 === 4 && t2 === 10)
    || (t1 === 10 && t2 === 4)
    || (t1 === 5 && t2 === 11)
    || (t1 === 11 && t2 === 5)
    || (t1 === 6 && t2 === 12)
    || (t1 === 12 && t2 === 6)) {
    return true
  }
  return false
}

export function calcRelate(t1, t2) {
  // 120 180 60 90
  return {
    f3m: calc120b(t1, t2),
    f180k: calc180b(t1, t2),
    ftopbottom: calctopbottom(t1, t2),
    f3h: calc0b(t1, t2),
    f6m: calc60b(t1, t2),
    // calcn60b(t1, t2) ? '星六合' : false,
    f90m: calc90b(t1, t2),
    f150m: calc150b(t1, t2),
    f60m: calc60cb(t1, t2),
  }
}
export function calcTRelate(t1, t2) {
  // 120 180 60 90
  return {
    f5m: calcTMerge(t1, t2),
  }
}
export function getTRelate(year, mon, date, hour, min, mode, sex, curyr, cmon, cday, chr, ctime) {
  const cData = {
    year: convToNextDay(year, mon, date, hour, min, mode)[0],
    mon: convToNextDay(year, mon, date, hour, min, mode)[1],
    date: convToNextDay(year, mon, date, hour, min, mode)[2],
    hour: convToNextDay(year, mon, date, hour, min, mode)[3],
    min: convToNextDay(year, mon, date, hour, min, mode)[4],
  }
  const cdyData = {
    year: convToNextDay(curyr, cmon, cday, chr, ctime, mode)[0],
    mon: convToNextDay(curyr, cmon, cday, chr, ctime, mode)[1],
    date: convToNextDay(curyr, cmon, cday, chr, ctime, mode)[2],
    hour: convToNextDay(curyr, cmon, cday, chr, ctime, mode)[3],
    min: convToNextDay(curyr, cmon, cday, chr, ctime, mode)[4],
  }
  const town = getFourTown(cData.year,
    cData.mon,
    cData.date,
    cData.hour,
    cData.min,
    mode)
  const dytown = getDynamicBigChannelTown(cData.year,
    cData.mon,
    cData.date,
    cData.hour,
    cData.min,
    mode,
    sex,
    curyr)
  const curdytown = getFourTown(cdyData.year,
    cdyData.mon,
    cdyData.date,
    cdyData.hour,
    cdyData.min,
    mode)
  return [
    calcTRelate(town[0][0], town[1][0]),
    calcTRelate(town[0][0], town[2][0]),
    calcTRelate(town[1][0], town[2][0]),
    calcTRelate(town[0][0], town[3][0]),
    calcTRelate(town[1][0], town[3][0]),
    calcTRelate(town[2][0], town[3][0]),
    calcTRelate(town[0][0], dytown[0]),
    calcTRelate(town[1][0], dytown[0]),
    calcTRelate(town[2][0], dytown[0]),
    calcTRelate(town[3][0], dytown[0]),
    calcTRelate(town[0][0], curdytown[0][0]),
    calcTRelate(town[1][0], curdytown[0][0]),
    calcTRelate(town[2][0], curdytown[0][0]),
    calcTRelate(town[3][0], curdytown[0][0]),
    calcTRelate(dytown[0], curdytown[0][0]),
    calcTRelate(town[0][0], curdytown[1][0]),
    calcTRelate(town[1][0], curdytown[1][0]),
    calcTRelate(town[2][0], curdytown[1][0]),
    calcTRelate(town[3][0], curdytown[1][0]),
    calcTRelate(dytown[0], curdytown[1][0]),
    calcTRelate(curdytown[0][0], curdytown[1][0]),
    calcTRelate(town[0][0], curdytown[2][0]),
    calcTRelate(town[1][0], curdytown[2][0]),
    calcTRelate(town[2][0], curdytown[2][0]),
    calcTRelate(town[3][0], curdytown[2][0]),
    calcTRelate(dytown[0], curdytown[2][0]),
    calcTRelate(curdytown[0][0], curdytown[2][0]),
    calcTRelate(curdytown[1][0], curdytown[2][0]),
    calcTRelate(town[0][0], curdytown[3][0]),
    calcTRelate(town[1][0], curdytown[3][0]),
    calcTRelate(town[2][0], curdytown[3][0]),
    calcTRelate(town[3][0], curdytown[3][0]),
    calcTRelate(dytown[0], curdytown[3][0]),
    calcTRelate(curdytown[0][0], curdytown[3][0]),
    calcTRelate(curdytown[1][0], curdytown[3][0]),
    calcTRelate(curdytown[2][0], curdytown[3][0]),
  ]
}
export function getBRelate(year, mon, date, hour, min, mode, sex, curyr, cmon, cday, chr, ctime) {
  const cData = {
    year: convToNextDay(year, mon, date, hour, min, mode)[0],
    mon: convToNextDay(year, mon, date, hour, min, mode)[1],
    date: convToNextDay(year, mon, date, hour, min, mode)[2],
    hour: convToNextDay(year, mon, date, hour, min, mode)[3],
    min: convToNextDay(year, mon, date, hour, min, mode)[4],
  }
  const cdyData = {
    year: convToNextDay(curyr, cmon, cday, chr, ctime, mode)[0],
    mon: convToNextDay(curyr, cmon, cday, chr, ctime, mode)[1],
    date: convToNextDay(curyr, cmon, cday, chr, ctime, mode)[2],
    hour: convToNextDay(curyr, cmon, cday, chr, ctime, mode)[3],
    min: convToNextDay(curyr, cmon, cday, chr, ctime, mode)[4],
  }
  const town = getFourTown(cData.year,
    cData.mon,
    cData.date,
    cData.hour,
    cData.min,
    mode)
  const dytown = getDynamicBigChannelTown(cData.year,
    cData.mon,
    cData.date,
    cData.hour,
    cData.min,
    mode,
    sex,
    curyr)
  const curdytown = getFourTown(cdyData.year,
    cdyData.mon,
    cdyData.date,
    cdyData.hour,
    cdyData.min,
    mode)
  return [
    calcRelate(town[0][1], town[1][1]),
    calcRelate(town[0][1], town[2][1]),
    calcRelate(town[1][1], town[2][1]),
    calcRelate(town[0][1], town[3][1]),
    calcRelate(town[1][1], town[3][1]),
    calcRelate(town[2][1], town[3][1]),
    calcRelate(town[0][1], dytown[1]),
    calcRelate(town[1][1], dytown[1]),
    calcRelate(town[2][1], dytown[1]),
    calcRelate(town[3][1], dytown[1]),
    calcRelate(town[0][1], curdytown[0][1]),
    calcRelate(town[1][1], curdytown[0][1]),
    calcRelate(town[2][1], curdytown[0][1]),
    calcRelate(town[3][1], curdytown[0][1]),
    calcRelate(dytown[1], curdytown[0][1]),
    calcRelate(town[0][1], curdytown[1][1]),
    calcRelate(town[1][1], curdytown[1][1]),
    calcRelate(town[2][1], curdytown[1][1]),
    calcRelate(town[3][1], curdytown[1][1]),
    calcRelate(dytown[1], curdytown[1][1]),
    calcRelate(curdytown[0][1], curdytown[1][1]),
    calcRelate(town[0][1], curdytown[2][1]),
    calcRelate(town[1][1], curdytown[2][1]),
    calcRelate(town[2][1], curdytown[2][1]),
    calcRelate(town[3][1], curdytown[2][1]),
    calcRelate(dytown[1], curdytown[2][1]),
    calcRelate(curdytown[0][1], curdytown[2][1]),
    calcRelate(curdytown[1][1], curdytown[2][1]),
    calcRelate(town[0][1], curdytown[3][1]),
    calcRelate(town[1][1], curdytown[3][1]),
    calcRelate(town[2][1], curdytown[3][1]),
    calcRelate(town[3][1], curdytown[3][1]),
    calcRelate(dytown[1], curdytown[3][1]),
    calcRelate(curdytown[0][1], curdytown[3][1]),
    calcRelate(curdytown[1][1], curdytown[3][1]),
    calcRelate(curdytown[2][1], curdytown[3][1]),
  ]
}

export function getFourValue(year, mon, date, hour, min, mode) {
  const cData = {
    year: convToNextDay(year, mon, date, hour, min, mode)[0],
    mon: convToNextDay(year, mon, date, hour, min, mode)[1],
    date: convToNextDay(year, mon, date, hour, min, mode)[2],
    hour: convToNextDay(year, mon, date, hour, min, mode)[3],
    min: convToNextDay(year, mon, date, hour, min, mode)[4],
  }
  const data = [
    getUserYearTown(cData.year, cData.mon, cData.date, cData.hour, cData.min),
    getUserMonthTown(convertToLocalTime(cData.year, cData.mon, cData.date, cData.hour, cData.min),
      get24EventDateList(getUserUseSpringDay(cData.year, cData.mon, cData.date, cData.hour, cData.min))),
    getDayTown(cData.year, cData.mon, cData.date),
    getUserTimeTown(getDayTown(cData.year, cData.mon, cData.date, 2),
      convertToLocalTime(cData.year, cData.mon, cData.date, cData.hour, cData.min)),
  ]

  data[0][0] = getWordPowerT(data[0][0])
  data[0][1] = getWordPowerB(data[0][1])
  data[1][0] = getWordPowerT(data[1][0])
  data[1][1] = getWordPowerB(data[1][1])
  data[2][0] = getWordPowerT(data[2][0])
  data[2][1] = getWordPowerB(data[2][1])
  data[3][0] = getWordPowerT(data[3][0])
  data[3][1] = getWordPowerB(data[3][1])

  return data
}

export function getTotalTownM(yr, mon, day, hr, time, mode, sex, curyr, cm, cd, ch, ct) {
  const curUser = getFourTown(parseInt(yr, 10),
    parseInt(mon, 10),
    parseInt(day, 10),
    parseInt(hr, 10),
    parseInt(time, 10),
    mode)
  const curBig = getDynamicBigChannelTown(parseInt(yr, 10),
    parseInt(mon, 10),
    parseInt(day, 10),
    parseInt(hr, 10),
    parseInt(time, 10),
    mode, sex, parseInt(curyr, 10))
  const curYear = getFourTown(parseInt(curyr, 10),
    parseInt(cm, 10),
    parseInt(cd, 10),
    parseInt(ch, 10),
    parseInt(ct, 10),
    mode)
  return [curUser[0], curUser[1], curUser[2], curUser[3], curBig,
    curYear[0], curYear[1], curYear[2], curYear[3]]
}

export function gettoptowncalc(wt, disnum, orgyr, orgmon, orgdate, orghr, orgtime, checked, sex, curyr, curmon, curdate, curhr, curtime) {
  let asf = []
  let withoutT = false
  if (wt === 1) {
    withoutT = false
  } else {
    withoutT = true
  }
  // debugger
  const ada = [false, false, false, false, false, false, false, false, false]
  const adfa = [false, false, false, false, false, false, false, false, false]
  const aa = getTotalTownM(orgyr, orgmon, orgdate,
    orghr, orgtime, checked, sex,
    curyr, curmon, curdate, curhr, curtime)
  asf = [aa[0], aa[1], aa[2], aa[3], aa[4], aa[5],
    aa[6], aa[7], aa[8]]
  const obj = getTRelate(parseInt(orgyr, 10),
    parseInt(orgmon, 10),
    parseInt(orgdate, 10),
    parseInt(orghr, 10),
    parseInt(orgtime, 10),
    checked,
    sex,
    parseInt(curyr, 10),
    parseInt(curmon, 10),
    parseInt(curdate, 10),
    parseInt(curhr, 10),
    parseInt(curtime, 10))

  if (obj[0].f5m) {
    // 年月
    adfa[0] = true
    adfa[1] = true
    asf[0] = calc5Merge(aa[0][0], aa[0][1], false)
    asf[1] = calc5Merge(aa[1][0], aa[1][1], false)
    if (asf[0][0] !== aa[0][0]) {
      ada[0] = true
    }
    if (asf[1][0] !== aa[1][0]) {
      ada[1] = true
    }
  }
  if (obj[1].f5m) {
    // 年日
    adfa[0] = true
    adfa[2] = true
    asf[0] = calc5Merge(aa[0][0], aa[0][1], false)
    asf[2] = calc5Merge(aa[2][0], aa[2][1], true)
    if (asf[0][0] !== aa[0][0]) {
      ada[0] = true
    }
    if (asf[2][0] !== aa[2][0]) {
      ada[2] = true
    }
  }
  if (obj[2].f5m) {
    // 月日
    adfa[1] = true
    adfa[2] = true
    asf[1] = calc5Merge(aa[1][0], aa[1][1], false)
    asf[2] = calc5Merge(aa[2][0], aa[2][1], true)
    if (asf[1][0] !== aa[1][0]) {
      ada[1] = true
    }
  }
  if (obj[3].f5m && !withoutT) {
    // 年時
    adfa[0] = true
    adfa[3] = true
    asf[0] = calc5Merge(aa[0][0], aa[0][1], false)
    asf[3] = calc5Merge(aa[3][0], aa[3][1], false)
    if (asf[3][0] !== aa[3][0]) {
      ada[3] = true
    }
    if (asf[0][0] !== aa[0][0]) {
      ada[0] = true
    }
  }
  if (obj[4].f5m && !withoutT) {
    // 月時
    adfa[1] = true
    adfa[3] = true
    asf[1] = calc5Merge(aa[1][0], aa[1][1], false)
    asf[3] = calc5Merge(aa[3][0], aa[3][1], false)
    if (asf[3][0] !== aa[3][0]) {
      ada[3] = true
    }
    if (asf[1][0] !== aa[1][0]) {
      ada[1] = true
    }
  }
  if (obj[5].f5m && !withoutT) {
    // 日時
    adfa[2] = true
    adfa[3] = true
    asf[2] = calc5Merge(aa[2][0], aa[2][1], true)
    asf[3] = calc5Merge(aa[3][0], aa[3][1], false)
    if (asf[3][0] !== aa[3][0]) {
      ada[3] = true
    }
  }

  if (obj[6].f5m && disnum > 4) {
    // 年大
    adfa[0] = true
    adfa[4] = true
    asf[0] = calc5Merge(aa[0][0], aa[0][1], false)
    asf[4] = calc5Merge(aa[4][0], aa[4][1], false)
    if (asf[0][0] !== aa[0][0]) {
      ada[0] = true
    }
    if (asf[4][0] !== aa[4][0]) {
      ada[4] = true
    }
  }
  if (obj[7].f5m && disnum > 4) {
    // 月大
    adfa[1] = true
    adfa[4] = true
    asf[1] = calc5Merge(aa[1][0], aa[1][1], false)
    asf[4] = calc5Merge(aa[4][0], aa[4][1], false)
    if (asf[1][0] !== aa[1][0]) {
      ada[1] = true
    }
    if (asf[4][0] !== aa[4][0]) {
      ada[4] = true
    }
  }
  if (obj[8].f5m && disnum > 4) {
    // 日大
    adfa[2] = true
    adfa[4] = true
    asf[2] = calc5Merge(aa[2][0], aa[2][1], true)
    asf[4] = calc5Merge(aa[4][0], aa[4][1], false)
    if (asf[2][0] !== aa[2][0]) {
      ada[2] = true
    }
    if (asf[4][0] !== aa[4][0]) {
      ada[4] = true
    }
  }
  if (obj[9].f5m && !withoutT && disnum > 4) {
    // 時大
    adfa[3] = true
    adfa[4] = true
    asf[3] = calc5Merge(aa[3][0], aa[3][1], false)
    asf[4] = calc5Merge(aa[4][0], aa[4][1], false)
    if (asf[3][0] !== aa[3][0]) {
      ada[3] = true
    }
    if (asf[4][0] !== aa[4][0]) {
      ada[4] = true
    }
  }
  if (obj[10].f5m && disnum > 5) {
    // 年L年
    adfa[0] = true
    adfa[5] = true
    asf[0] = calc5Merge(aa[0][0], aa[0][1], false)
    asf[5] = calc5Merge(aa[5][0], aa[5][1], false)
    if (asf[0][0] !== aa[0][0]) {
      ada[0] = true
    }
    if (asf[5][0] !== aa[5][0]) {
      ada[5] = true
    }
  }
  if (obj[11].f5m && disnum > 5) {
    // 月L年
    adfa[1] = true
    adfa[5] = true
    asf[1] = calc5Merge(aa[1][0], aa[1][1], false)
    asf[5] = calc5Merge(aa[5][0], aa[5][1], false)
    if (asf[1][0] !== aa[1][0]) {
      ada[1] = true
    }
    if (asf[5][0] !== aa[5][0]) {
      ada[5] = true
    }
  }
  if (obj[12].f5m && disnum > 5) {
    // 日L年
    adfa[2] = true
    adfa[5] = true
    asf[2] = calc5Merge(aa[2][0], aa[2][1], true)
    asf[5] = calc5Merge(aa[5][0], aa[5][1], false)
    if (asf[2][0] !== aa[2][0]) {
      ada[2] = true
    }
    if (asf[5][0] !== aa[5][0]) {
      ada[5] = true
    }
  }
  if (obj[13].f5m && !withoutT && !disnum > 5) {
    // 時L年
    adfa[3] = true
    adfa[5] = true
    asf[3] = calc5Merge(aa[3][0], aa[3][1], false)
    asf[5] = calc5Merge(aa[5][0], aa[5][1], false)
    if (asf[3][0] !== aa[3][0]) {
      ada[3] = true
    }
    if (asf[5][0] !== aa[5][0]) {
      ada[5] = true
    }
  }
  if (obj[14].f5m && disnum > 5) {
    // 大L年
    adfa[4] = true
    adfa[5] = true
    asf[4] = calc5Merge(aa[4][0], aa[4][1], false)
    asf[5] = calc5Merge(aa[5][0], aa[5][1], false)
    if (asf[4][0] !== aa[4][0]) {
      ada[4] = true
    }
    if (asf[5][0] !== aa[5][0]) {
      ada[5] = true
    }
  }

  if (obj[15].f5m && disnum > 6) {
    // 年L月
    adfa[0] = true
    adfa[6] = true
    asf[0] = calc5Merge(aa[0][0], aa[0][1], false)
    asf[6] = calc5Merge(aa[6][0], aa[6][1], false)
    if (asf[6][0] !== aa[6][0]) {
      ada[6] = true
    }
    if (asf[0][0] !== aa[0][0]) {
      ada[0] = true
    }
  }
  if (obj[16].f5m && disnum > 6) {
    // 月L月
    adfa[1] = true
    adfa[6] = true
    asf[1] = calc5Merge(aa[1][0], aa[1][1], false)
    asf[6] = calc5Merge(aa[6][0], aa[6][1], false)
    if (asf[6][0] !== aa[6][0]) {
      ada[6] = true
    }
    if (asf[1][0] !== aa[1][0]) {
      ada[1] = true
    }
  }
  if (obj[17].f5m && disnum > 6) {
    // 日L月
    adfa[2] = true
    adfa[6] = true
    asf[2] = calc5Merge(aa[2][0], aa[2][1], true)
    asf[6] = calc5Merge(aa[6][0], aa[6][1], false)
    if (asf[6][0] !== aa[6][0]) {
      ada[6] = true
    }
    if (asf[2][0] !== aa[2][0]) {
      ada[2] = true
    }
  }
  if (obj[18].f5m && !withoutT && disnum > 6) {
    // 時L月
    adfa[3] = true
    adfa[6] = true
    asf[3] = calc5Merge(aa[3][0], aa[3][1], false)
    asf[6] = calc5Merge(aa[6][0], aa[6][1], false)
    if (asf[6][0] !== aa[6][0]) {
      ada[6] = true
    }
    if (asf[3][0] !== aa[3][0]) {
      ada[3] = true
    }
  }
  if (obj[19].f5m && disnum > 6) {
    // 大L月
    adfa[4] = true
    adfa[6] = true
    asf[4] = calc5Merge(aa[4][0], aa[4][1], false)
    asf[6] = calc5Merge(aa[6][0], aa[6][1], false)
    if (asf[6][0] !== aa[6][0]) {
      ada[6] = true
    }
    if (asf[4][0] !== aa[4][0]) {
      ada[4] = true
    }
  }
  if (obj[20].f5m && disnum > 6) {
    // L年月
    adfa[5] = true
    adfa[6] = true
    asf[5] = calc5Merge(aa[5][0], aa[5][1], false)
    asf[6] = calc5Merge(aa[6][0], aa[6][1], false)
    if (asf[6][0] !== aa[6][0]) {
      ada[6] = true
    }
    if (asf[5][0] !== aa[5][0]) {
      ada[5] = true
    }
  }

  if (obj[21].f5m && disnum > 7) {
    // 年L日
    adfa[0] = true
    adfa[7] = true
    asf[0] = calc5Merge(aa[0][0], aa[0][1], false)
    asf[7] = calc5Merge(aa[7][0], aa[7][1], false)
    if (asf[0][0] !== aa[0][0]) {
      ada[0] = true
    }
    if (asf[7][0] !== aa[7][0]) {
      ada[7] = true
    }
  }
  if (obj[22].f5m && disnum > 7) {
    // 月L日
    adfa[1] = true
    adfa[7] = true
    asf[1] = calc5Merge(aa[1][0], aa[1][1], false)
    asf[7] = calc5Merge(aa[7][0], aa[7][1], false)
    if (asf[1][0] !== aa[1][0]) {
      ada[1] = true
    }
    if (asf[7][0] !== aa[7][0]) {
      ada[7] = true
    }
  }
  if (obj[23].f5m && disnum > 7) {
    // 日L日
    adfa[2] = true
    adfa[7] = true
    asf[2] = calc5Merge(aa[2][0], aa[2][1], true)
    asf[7] = calc5Merge(aa[7][0], aa[7][1], false)
    if (asf[2][0] !== aa[2][0]) {
      ada[2] = true
    }
    if (asf[7][0] !== aa[7][0]) {
      ada[7] = true
    }
  }
  if (obj[24].f5m && !withoutT && disnum > 7) {
    // 時L日
    adfa[3] = true
    adfa[7] = true
    asf[3] = calc5Merge(aa[3][0], aa[3][1], false)
    asf[7] = calc5Merge(aa[7][0], aa[7][1], false)
    if (asf[3][0] !== aa[3][0]) {
      ada[3] = true
    }
    if (asf[7][0] !== aa[7][0]) {
      ada[7] = true
    }
  }
  if (obj[25].f5m && disnum > 7) {
    // 大L日
    adfa[4] = true
    adfa[7] = true
    asf[4] = calc5Merge(aa[4][0], aa[4][1], false)
    asf[7] = calc5Merge(aa[7][0], aa[7][1], false)
    if (asf[4][0] !== aa[4][0]) {
      ada[4] = true
    }
    if (asf[7][0] !== aa[7][0]) {
      ada[7] = true
    }
  }
  if (obj[26].f5m && disnum > 7) {
    // L年日
    adfa[5] = true
    adfa[7] = true
    asf[5] = calc5Merge(aa[5][0], aa[5][1], false)
    asf[7] = calc5Merge(aa[7][0], aa[7][1], false)
    if (asf[5][0] !== aa[5][0]) {
      ada[5] = true
    }
    if (asf[7][0] !== aa[7][0]) {
      ada[7] = true
    }
  }
  if (obj[27].f5m && disnum > 7) {
    // L月日
    adfa[6] = true
    adfa[7] = true
    asf[6] = calc5Merge(aa[6][0], aa[6][1], false)
    asf[7] = calc5Merge(aa[7][0], aa[7][1], false)
    if (asf[6][0] !== aa[6][0]) {
      ada[6] = true
    }
    if (asf[7][0] !== aa[7][0]) {
      ada[7] = true
    }
  }

  if (obj[28].f5m && disnum > 8) {
    // L日時
    adfa[0] = true
    adfa[8] = true
    asf[0] = calc5Merge(aa[0][0], aa[0][1], false)
    asf[8] = calc5Merge(aa[8][0], aa[8][1], false)
    if (asf[0][0] !== aa[0][0]) {
      ada[0] = true
    }
    if (asf[8][0] !== aa[8][0]) {
      ada[8] = true
    }
  }
  if (obj[29].f5m && disnum > 8) {
    // L日時
    adfa[1] = true
    adfa[8] = true
    asf[1] = calc5Merge(aa[1][0], aa[1][1], false)
    asf[8] = calc5Merge(aa[8][0], aa[8][1], false)
    if (asf[1][0] !== aa[1][0]) {
      ada[1] = true
    }
    if (asf[8][0] !== aa[8][0]) {
      ada[8] = true
    }
  }
  if (obj[30].f5m && disnum > 8) {
    // L日時
    adfa[2] = true
    adfa[8] = true
    asf[2] = calc5Merge(aa[2][0], aa[2][1], true)
    asf[8] = calc5Merge(aa[8][0], aa[8][1], false)
    if (asf[2][0] !== aa[2][0]) {
      ada[2] = true
    }
    if (asf[8][0] !== aa[8][0]) {
      ada[8] = true
    }
  }
  if (obj[31].f5m && !withoutT && disnum > 8) {
    // L日時
    adfa[3] = true
    adfa[8] = true
    asf[3] = calc5Merge(aa[3][0], aa[3][1], false)
    asf[8] = calc5Merge(aa[8][0], aa[8][1], false)
    if (asf[3][0] !== aa[3][0]) {
      ada[3] = true
    }
    if (asf[8][0] !== aa[8][0]) {
      ada[8] = true
    }
  }
  if (obj[32].f5m && disnum > 8) {
    // L日時
    adfa[4] = true
    adfa[8] = true
    asf[4] = calc5Merge(aa[4][0], aa[4][1], false)
    asf[8] = calc5Merge(aa[8][0], aa[8][1], false)
    if (asf[4][0] !== aa[4][0]) {
      ada[4] = true
    }
    if (asf[8][0] !== aa[8][0]) {
      ada[8] = true
    }
  }
  if (obj[33].f5m && disnum > 8) {
    // L日時
    adfa[5] = true
    adfa[8] = true
    asf[5] = calc5Merge(aa[5][0], aa[5][1], false)
    asf[8] = calc5Merge(aa[8][0], aa[8][1], false)
    if (asf[5][0] !== aa[5][0]) {
      ada[5] = true
    }
    if (asf[8][0] !== aa[8][0]) {
      ada[8] = true
    }
  }
  if (obj[34].f5m && disnum > 8) {
    // L日時
    adfa[6] = true
    adfa[8] = true
    asf[6] = calc5Merge(aa[6][0], aa[6][1], false)
    asf[8] = calc5Merge(aa[8][0], aa[8][1], false)
    if (asf[6][0] !== aa[6][0]) {
      ada[6] = true
    }
    if (asf[8][0] !== aa[8][0]) {
      ada[8] = true
    }
  }
  if (obj[35].f5m && disnum > 8) {
    // L日時
    adfa[7] = true
    adfa[8] = true
    asf[7] = calc5Merge(aa[7][0], aa[7][1], false)
    asf[8] = calc5Merge(aa[8][0], aa[8][1], false)
    if (asf[7][0] !== aa[7][0]) {
      ada[7] = true
    }
    if (asf[8][0] !== aa[8][0]) {
      ada[8] = true
    }
  }

  return [asf, ada, adfa]
}

export function getbottomtowncalc(wt, disnum, orgyr, orgmon, orgdate, orghr, orgtime, checked, sex, curyr, curmon, curdate, curhr, curtime) {
  let asf = []
  let withoutT = false
  if (wt === 1) {
    withoutT = false
  } else {
    withoutT = true
  }
  const mergeIn3 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
  const merginIn3Grp = [0, 0, 0, 0, 0, 0, 0, 0, 0]
  const mergeIn6 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
  const push180 = [false, false, false, false, false, false, false, false, false]
  const pushtopbottom = [false, false, false, false, false, false, false, false, false]
  const push90 = [false, false, false, false, false, false, false, false, false]
  const push150 = [false, false, false, false, false, false, false, false, false]
  const push60 = [false, false, false, false, false, false, false, false, false]
  const aa = getTotalTownM(orgyr, orgmon, orgdate,
    orghr, orgtime, checked, sex,
    curyr, curmon, curdate, curhr, curtime)
  asf = [aa[0], aa[1], aa[2], aa[3], aa[4], aa[5],
    aa[6], aa[7], aa[8]]
  const obj = getBRelate(parseInt(orgyr, 10),
    parseInt(orgmon, 10),
    parseInt(orgdate, 10),
    parseInt(orghr, 10),
    parseInt(orgtime, 10),
    checked,
    sex,
    parseInt(curyr, 10),
    parseInt(curmon, 10),
    parseInt(curdate, 10),
    parseInt(curhr, 10),
    parseInt(curtime, 10))
  if (obj[0].f3m) {
    // 年月
    mergeIn3[0] = calc3MType(aa[0][1])
    mergeIn3[1] = calc3MType(aa[1][1])
  }
  if (obj[1].f3m) {
    // 年日
    mergeIn3[2] = calc3MType(aa[2][1])
    mergeIn3[0] = calc3MType(aa[0][1])
  }
  if (obj[2].f3m) {
    // 月日
    mergeIn3[2] = calc3MType(aa[2][1])
    mergeIn3[1] = calc3MType(aa[1][1])
  }
  if (obj[3].f3m && !withoutT) {
    // 年時
    mergeIn3[0] = calc3MType(aa[0][1])
    mergeIn3[3] = calc3MType(aa[3][1])
  }
  if (obj[4].f3m && !withoutT) {
    // 月時
    mergeIn3[1] = calc3MType(aa[1][1])
    mergeIn3[3] = calc3MType(aa[3][1])
  }
  if (obj[5].f3m && !withoutT) {
    // 日時
    mergeIn3[2] = calc3MType(aa[2][1])
    mergeIn3[3] = calc3MType(aa[3][1])
  }
  if (obj[6].f3m && disnum > 4) {
    // 年大
    mergeIn3[0] = calc3MType(aa[0][1])
    mergeIn3[4] = calc3MType(aa[4][1])
  }
  if (obj[7].f3m && disnum > 4) {
    // 月大
    mergeIn3[1] = calc3MType(aa[1][1])
    mergeIn3[4] = calc3MType(aa[4][1])
  }
  if (obj[8].f3m && disnum > 4) {
    // 日大
    mergeIn3[2] = calc3MType(aa[2][1])
    mergeIn3[4] = calc3MType(aa[4][1])
  }
  if (obj[9].f3m && !withoutT && disnum > 4) {
    // 時大
    mergeIn3[3] = calc3MType(aa[3][1])
    mergeIn3[4] = calc3MType(aa[4][1])
  }
  if (obj[10].f3m && disnum > 5) {
    // 年L年
    mergeIn3[0] = calc3MType(aa[0][1])
    mergeIn3[5] = calc3MType(aa[5][1])
  }
  if (obj[11].f3m && disnum > 5) {
    // 月L年
    mergeIn3[1] = calc3MType(aa[1][1])
    mergeIn3[5] = calc3MType(aa[5][1])
  }
  if (obj[12].f3m && disnum > 5) {
    // 日L年
    mergeIn3[2] = calc3MType(aa[2][1])
    mergeIn3[5] = calc3MType(aa[5][1])
  }
  if (obj[13].f3m && !withoutT && disnum > 5) {
    // 時L年
    mergeIn3[3] = calc3MType(aa[3][1])
    mergeIn3[5] = calc3MType(aa[5][1])
  }
  if (obj[14].f3m && disnum > 5) {
    // 大L年
    mergeIn3[4] = calc3MType(aa[4][1])
    mergeIn3[5] = calc3MType(aa[5][1])
  }
  if (obj[15].f3m && disnum > 6) {
    // 年L月
    mergeIn3[0] = calc3MType(aa[0][1])
    mergeIn3[6] = calc3MType(aa[6][1])
  }
  if (obj[16].f3m && disnum > 6) {
    // 月L月
    mergeIn3[1] = calc3MType(aa[1][1])
    mergeIn3[6] = calc3MType(aa[6][1])
  }
  if (obj[17].f3m && disnum > 6) {
    // 日L月
    mergeIn3[2] = calc3MType(aa[2][1])
    mergeIn3[6] = calc3MType(aa[6][1])
  }
  if (obj[18].f3m && !withoutT && disnum > 6) {
    // 時L月
    mergeIn3[3] = calc3MType(aa[3][1])
    mergeIn3[6] = calc3MType(aa[6][1])
  }
  if (obj[19].f3m && disnum > 6) {
    // 大L月
    mergeIn3[4] = calc3MType(aa[4][1])
    mergeIn3[6] = calc3MType(aa[6][1])
  }
  if (obj[20].f3m && disnum > 6) {
    // L年月
    mergeIn3[5] = calc3MType(aa[5][1])
    mergeIn3[6] = calc3MType(aa[6][1])
  }
  if (obj[21].f3m && disnum > 7) {
    // 年L日
    mergeIn3[0] = calc3MType(aa[0][1])
    mergeIn3[7] = calc3MType(aa[7][1])
  }
  if (obj[22].f3m && disnum > 7) {
    // 月L日
    mergeIn3[1] = calc3MType(aa[1][1])
    mergeIn3[7] = calc3MType(aa[7][1])
  }
  if (obj[23].f3m && disnum > 7) {
    // 日L日
    mergeIn3[2] = calc3MType(aa[2][1])
    mergeIn3[7] = calc3MType(aa[7][1])
  }
  if (obj[24].f3m && !withoutT && disnum > 7) {
    // 時L日
    mergeIn3[3] = calc3MType(aa[3][1])
    mergeIn3[7] = calc3MType(aa[7][1])
  }
  if (obj[25].f3m && disnum > 7) {
    // 大L日
    mergeIn3[4] = calc3MType(aa[4][1])
    mergeIn3[7] = calc3MType(aa[7][1])
  }
  if (obj[26].f3m && disnum > 7) {
    // L年日
    mergeIn3[5] = calc3MType(aa[5][1])
    mergeIn3[7] = calc3MType(aa[7][1])
  }
  if (obj[27].f3m && disnum > 7) {
    // L月日
    mergeIn3[6] = calc3MType(aa[6][1])
    mergeIn3[7] = calc3MType(aa[7][1])
  }
  if (obj[28].f3m && disnum > 8) {
    // 年L時
    mergeIn3[0] = calc3MType(aa[0][1])
    mergeIn3[8] = calc3MType(aa[8][1])
  }
  if (obj[29].f3m && disnum > 8) {
    // 月L時
    mergeIn3[1] = calc3MType(aa[1][1])
    mergeIn3[8] = calc3MType(aa[8][1])
  }
  if (obj[30].f3m && disnum > 8) {
    // 日L時
    mergeIn3[2] = calc3MType(aa[2][1])
    mergeIn3[8] = calc3MType(aa[8][1])
  }
  if (obj[31].f3m && disnum > 8) {
    // 時L時
    mergeIn3[3] = calc3MType(aa[3][1])
    mergeIn3[8] = calc3MType(aa[8][1])
  }
  if (obj[32].f3m && disnum > 8) {
    // 大L時
    mergeIn3[4] = calc3MType(aa[4][1])
    mergeIn3[8] = calc3MType(aa[8][1])
  }
  if (obj[33].f3m && disnum > 8) {
    // L年時
    mergeIn3[5] = calc3MType(aa[5][1])
    mergeIn3[8] = calc3MType(aa[8][1])
  }
  if (obj[34].f3m && disnum > 8) {
    // L月時
    mergeIn3[6] = calc3MType(aa[6][1])
    mergeIn3[8] = calc3MType(aa[8][1])
  }
  if (obj[35].f3m && disnum > 8) {
    // L日時
    mergeIn3[7] = calc3MType(aa[7][1])
    mergeIn3[8] = calc3MType(aa[8][1])
  }

  if (obj[0].f3h) {
    // 年月
    merginIn3Grp[0] = calc3GType(aa[0][1])
    merginIn3Grp[1] = calc3GType(aa[1][1])
  }
  if (obj[1].f3h) {
    // 年日
    merginIn3Grp[2] = calc3GType(aa[2][1])
    merginIn3Grp[0] = calc3GType(aa[0][1])
  }
  if (obj[2].f3h) {
    // 月日
    merginIn3Grp[2] = calc3GType(aa[2][1])
    merginIn3Grp[1] = calc3GType(aa[1][1])
  }
  if (obj[3].f3h && !withoutT) {
    // 年時
    merginIn3Grp[0] = calc3GType(aa[0][1])
    merginIn3Grp[3] = calc3GType(aa[3][1])
  }
  if (obj[4].f3h && !withoutT) {
    // 月時
    merginIn3Grp[1] = calc3GType(aa[1][1])
    merginIn3Grp[3] = calc3GType(aa[3][1])
  }
  if (obj[5].f3h && !withoutT) {
    // 日時
    merginIn3Grp[2] = calc3GType(aa[2][1])
    merginIn3Grp[3] = calc3GType(aa[3][1])
  }
  if (obj[6].f3h && disnum > 4) {
    // 年大
    merginIn3Grp[0] = calc3GType(aa[0][1])
    merginIn3Grp[4] = calc3GType(aa[4][1])
  }
  if (obj[7].f3h && disnum > 4) {
    // 月大
    merginIn3Grp[1] = calc3GType(aa[1][1])
    merginIn3Grp[4] = calc3GType(aa[4][1])
  }
  if (obj[8].f3h && disnum > 4) {
    // 日大
    merginIn3Grp[2] = calc3GType(aa[2][1])
    merginIn3Grp[4] = calc3GType(aa[4][1])
  }
  if (obj[9].f3h && !withoutT && disnum > 4) {
    // 時大
    merginIn3Grp[3] = calc3GType(aa[3][1])
    merginIn3Grp[4] = calc3GType(aa[4][1])
  }
  if (obj[10].f3h && disnum > 5) {
    // 年L年
    merginIn3Grp[0] = calc3GType(aa[0][1])
    merginIn3Grp[5] = calc3GType(aa[5][1])
  }
  if (obj[11].f3h && disnum > 5) {
    // 月L年
    merginIn3Grp[1] = calc3GType(aa[1][1])
    merginIn3Grp[5] = calc3GType(aa[5][1])
  }
  if (obj[12].f3h && disnum > 5) {
    // 日L年
    merginIn3Grp[2] = calc3GType(aa[2][1])
    merginIn3Grp[5] = calc3GType(aa[5][1])
  }
  if (obj[13].f3h && !withoutT && disnum > 5) {
    // 時L年
    merginIn3Grp[3] = calc3GType(aa[3][1])
    merginIn3Grp[5] = calc3GType(aa[5][1])
  }
  if (obj[14].f3h && disnum > 5) {
    // 大L年
    merginIn3Grp[4] = calc3GType(aa[4][1])
    merginIn3Grp[5] = calc3GType(aa[5][1])
  }
  if (obj[15].f3h && disnum > 6) {
    // 年L月
    merginIn3Grp[0] = calc3GType(aa[0][1])
    merginIn3Grp[6] = calc3GType(aa[6][1])
  }
  if (obj[16].f3h && disnum > 6) {
    // 月L月
    merginIn3Grp[1] = calc3GType(aa[1][1])
    merginIn3Grp[6] = calc3GType(aa[6][1])
  }
  if (obj[17].f3h && disnum > 6) {
    // 日L月
    merginIn3Grp[2] = calc3GType(aa[2][1])
    merginIn3Grp[6] = calc3GType(aa[6][1])
  }
  if (obj[18].f3h && !withoutT && disnum > 6) {
    // 時L月
    merginIn3Grp[3] = calc3GType(aa[3][1])
    merginIn3Grp[6] = calc3GType(aa[6][1])
  }
  if (obj[19].f3h && disnum > 6) {
    // 大L月
    merginIn3Grp[4] = calc3GType(aa[4][1])
    merginIn3Grp[6] = calc3GType(aa[6][1])
  }
  if (obj[20].f3h && disnum > 6) {
    // L年月
    merginIn3Grp[5] = calc3GType(aa[5][1])
    merginIn3Grp[6] = calc3GType(aa[6][1])
  }
  if (obj[21].f3h && disnum > 7) {
    // 年L日
    merginIn3Grp[0] = calc3GType(aa[0][1])
    merginIn3Grp[7] = calc3GType(aa[7][1])
  }
  if (obj[22].f3h && disnum > 7) {
    // 月L日
    merginIn3Grp[1] = calc3GType(aa[1][1])
    merginIn3Grp[7] = calc3GType(aa[7][1])
  }
  if (obj[23].f3h && disnum > 7) {
    // 日L日
    merginIn3Grp[2] = calc3GType(aa[2][1])
    merginIn3Grp[7] = calc3GType(aa[7][1])
  }
  if (obj[24].f3h && !withoutT && disnum > 7) {
    // 時L日
    merginIn3Grp[3] = calc3GType(aa[3][1])
    merginIn3Grp[7] = calc3GType(aa[7][1])
  }
  if (obj[25].f3h && disnum > 7) {
    // 大L日
    merginIn3Grp[4] = calc3GType(aa[4][1])
    merginIn3Grp[7] = calc3GType(aa[7][1])
  }
  if (obj[26].f3h && disnum > 7) {
    // L年日
    merginIn3Grp[5] = calc3GType(aa[5][1])
    merginIn3Grp[7] = calc3GType(aa[7][1])
  }
  if (obj[27].f3h && disnum > 7) {
    // L月日
    merginIn3Grp[6] = calc3GType(aa[6][1])
    merginIn3Grp[7] = calc3GType(aa[7][1])
  }
  if (obj[28].f3h && disnum > 8) {
    // 年L時
    merginIn3Grp[0] = calc3GType(aa[0][1])
    merginIn3Grp[8] = calc3GType(aa[8][1])
  }
  if (obj[29].f3h && disnum > 8) {
    // 月L時
    merginIn3Grp[1] = calc3GType(aa[1][1])
    merginIn3Grp[8] = calc3GType(aa[8][1])
  }
  if (obj[30].f3h && disnum > 8) {
    // 日L時
    merginIn3Grp[2] = calc3GType(aa[2][1])
    merginIn3Grp[8] = calc3GType(aa[8][1])
  }
  if (obj[31].f3h && !withoutT && disnum > 8) {
    // 時L時
    merginIn3Grp[3] = calc3GType(aa[3][1])
    merginIn3Grp[8] = calc3GType(aa[8][1])
  }
  if (obj[32].f3h && disnum > 8) {
    // 大L時
    merginIn3Grp[4] = calc3GType(aa[4][1])
    merginIn3Grp[8] = calc3GType(aa[8][1])
  }
  if (obj[33].f3h && disnum > 8) {
    // L年時
    merginIn3Grp[5] = calc3GType(aa[5][1])
    merginIn3Grp[8] = calc3GType(aa[8][1])
  }
  if (obj[34].f3h && disnum > 8) {
    // L月時
    merginIn3Grp[6] = calc3GType(aa[6][1])
    merginIn3Grp[8] = calc3GType(aa[8][1])
  }
  if (obj[35].f3h && disnum > 8) {
    // L日時
    merginIn3Grp[7] = calc3GType(aa[7][1])
    merginIn3Grp[8] = calc3GType(aa[8][1])
  }

  if (obj[0].f6m) {
    // 年月
    mergeIn6[0] = calc6MType(aa[0][1])
    mergeIn6[1] = calc6MType(aa[1][1])
  }
  if (obj[1].f6m) {
    // 年日
    mergeIn6[2] = calc6MType(aa[2][1])
    mergeIn6[0] = calc6MType(aa[0][1])
  }
  if (obj[2].f6m) {
    // 月日
    mergeIn6[2] = calc6MType(aa[2][1])
    mergeIn6[1] = calc6MType(aa[1][1])
  }
  if (obj[3].f6m && !withoutT) {
    // 年時
    mergeIn6[0] = calc6MType(aa[0][1])
    mergeIn6[3] = calc6MType(aa[3][1])
  }
  if (obj[4].f6m && !withoutT) {
    // 月時
    mergeIn6[1] = calc6MType(aa[1][1])
    mergeIn6[3] = calc6MType(aa[3][1])
  }
  if (obj[5].f6m && !withoutT) {
    // 日時
    mergeIn6[2] = calc6MType(aa[2][1])
    mergeIn6[3] = calc6MType(aa[3][1])
  }
  if (obj[6].f6m && disnum > 4) {
    // 年大
    mergeIn6[0] = calc6MType(aa[0][1])
    mergeIn6[4] = calc6MType(aa[4][1])
  }
  if (obj[7].f6m && disnum > 4) {
    // 月大
    mergeIn6[1] = calc6MType(aa[1][1])
    mergeIn6[4] = calc6MType(aa[4][1])
  }
  if (obj[8].f6m && disnum > 4) {
    // 日大
    mergeIn6[2] = calc6MType(aa[2][1])
    mergeIn6[4] = calc6MType(aa[4][1])
  }
  if (obj[9].f6m && !withoutT && disnum > 4) {
    // 時大
    mergeIn6[3] = calc6MType(aa[3][1])
    mergeIn6[4] = calc6MType(aa[4][1])
  }
  if (obj[10].f6m && disnum > 5) {
    // 年L年
    mergeIn6[0] = calc6MType(aa[0][1])
    mergeIn6[5] = calc6MType(aa[5][1])
  }
  if (obj[11].f6m && disnum > 5) {
    // 月L年
    mergeIn6[1] = calc6MType(aa[1][1])
    mergeIn6[5] = calc6MType(aa[5][1])
  }
  if (obj[12].f6m && disnum > 5) {
    // 日L年
    mergeIn6[2] = calc6MType(aa[2][1])
    mergeIn6[5] = calc6MType(aa[5][1])
  }
  if (obj[13].f6m && !withoutT && disnum > 5) {
    // 時L年
    mergeIn6[3] = calc6MType(aa[3][1])
    mergeIn6[5] = calc6MType(aa[5][1])
  }
  if (obj[14].f6m && disnum > 5) {
    // 大L年
    mergeIn6[4] = calc6MType(aa[4][1])
    mergeIn6[5] = calc6MType(aa[5][1])
  }
  if (obj[15].f6m && disnum > 6) {
    // 年L月
    mergeIn6[0] = calc6MType(aa[0][1])
    mergeIn6[6] = calc6MType(aa[6][1])
  }
  if (obj[16].f6m && disnum > 6) {
    // 月L月
    mergeIn6[1] = calc6MType(aa[1][1])
    mergeIn6[6] = calc6MType(aa[6][1])
  }
  if (obj[17].f6m && disnum > 6) {
    // 日L月
    mergeIn6[2] = calc6MType(aa[2][1])
    mergeIn6[6] = calc6MType(aa[6][1])
  }
  if (obj[18].f6m && !withoutT && disnum > 6) {
    // 時L月
    mergeIn6[3] = calc6MType(aa[3][1])
    mergeIn6[6] = calc6MType(aa[6][1])
  }
  if (obj[19].f6m && disnum > 6) {
    // 大L月
    mergeIn6[4] = calc6MType(aa[4][1])
    mergeIn6[6] = calc6MType(aa[6][1])
  }
  if (obj[20].f6m && disnum > 6) {
    // L年月
    mergeIn6[5] = calc6MType(aa[5][1])
    mergeIn6[6] = calc6MType(aa[6][1])
  }
  if (obj[21].f6m && disnum > 7) {
    // 年L日
    mergeIn6[0] = calc6MType(aa[0][1])
    mergeIn6[7] = calc6MType(aa[7][1])
  }
  if (obj[22].f6m && disnum > 7) {
    // 月L日
    mergeIn6[1] = calc6MType(aa[1][1])
    mergeIn6[7] = calc6MType(aa[7][1])
  }
  if (obj[23].f6m && disnum > 7) {
    // 日L日
    mergeIn6[2] = calc6MType(aa[2][1])
    mergeIn6[7] = calc6MType(aa[7][1])
  }
  if (obj[24].f6m && !withoutT && disnum > 7) {
    // 時L日
    mergeIn6[3] = calc6MType(aa[3][1])
    mergeIn6[7] = calc6MType(aa[7][1])
  }
  if (obj[25].f6m && disnum > 7) {
    // 大L日
    mergeIn6[4] = calc6MType(aa[4][1])
    mergeIn6[7] = calc6MType(aa[7][1])
  }
  if (obj[26].f6m && disnum > 7) {
    // L年日
    mergeIn6[5] = calc6MType(aa[5][1])
    mergeIn6[7] = calc6MType(aa[7][1])
  }
  if (obj[27].f6m && disnum > 7) {
    // L月日
    mergeIn6[6] = calc6MType(aa[6][1])
    mergeIn6[7] = calc6MType(aa[7][1])
  }
  if (obj[28].f6m && disnum > 8) {
    // 年L時
    mergeIn6[0] = calc6MType(aa[0][1])
    mergeIn6[8] = calc6MType(aa[8][1])
  }
  if (obj[29].f6m && disnum > 8) {
    // 月L時
    mergeIn6[1] = calc6MType(aa[1][1])
    mergeIn6[8] = calc6MType(aa[8][1])
  }
  if (obj[30].f6m && disnum > 8) {
    // 日L時
    mergeIn6[2] = calc6MType(aa[2][1])
    mergeIn6[8] = calc6MType(aa[8][1])
  }
  if (obj[31].f6m && !withoutT && disnum > 8) {
    // 時L時
    mergeIn6[3] = calc6MType(aa[3][1])
    mergeIn6[8] = calc6MType(aa[8][1])
  }
  if (obj[32].f6m && disnum > 8) {
    // 大L時
    mergeIn6[4] = calc6MType(aa[4][1])
    mergeIn6[8] = calc6MType(aa[8][1])
  }
  if (obj[33].f6m && disnum > 8) {
    // L年時
    mergeIn6[5] = calc6MType(aa[5][1])
    mergeIn6[8] = calc6MType(aa[8][1])
  }
  if (obj[34].f6m && disnum > 8) {
    // L月時
    mergeIn6[6] = calc6MType(aa[6][1])
    mergeIn6[8] = calc6MType(aa[8][1])
  }
  if (obj[35].f6m && disnum > 8) {
    // L日時
    mergeIn6[7] = calc6MType(aa[7][1])
    mergeIn6[8] = calc6MType(aa[8][1])
  }

  if (obj[0].f180k) {
    // 年月
    push180[0] = true
    push180[1] = true
  }
  if (obj[1].f180k) {
    // 年日
    push180[2] = true
    push180[0] = true
  }
  if (obj[2].f180k) {
    // 月日
    push180[2] = true
    push180[1] = true
  }
  if (obj[3].f180k && !withoutT) {
    // 年時
    push180[0] = true
    push180[3] = true
  }
  if (obj[4].f180k && !withoutT) {
    // 月時
    push180[1] = true
    push180[3] = true
  }
  if (obj[5].f180k && !withoutT) {
    // 日時
    push180[2] = true
    push180[3] = true
  }
  if (obj[6].f180k && disnum > 4) {
    // 年大
    push180[0] = true
    push180[4] = true
  }
  if (obj[7].f180k && disnum > 4) {
    // 月大
    push180[1] = true
    push180[4] = true
  }
  if (obj[8].f180k && disnum > 4) {
    // 日大
    push180[2] = true
    push180[4] = true
  }
  if (obj[9].f180k && !withoutT && disnum > 4) {
    // 時大
    push180[3] = true
    push180[4] = true
  }
  if (obj[10].f180k && disnum > 5) {
    // 年L年
    push180[0] = true
    push180[5] = true
  }
  if (obj[11].f180k && disnum > 5) {
    // 月L年
    push180[1] = true
    push180[5] = true
  }
  if (obj[12].f180k && disnum > 5) {
    // 日L年
    push180[2] = true
    push180[5] = true
  }
  if (obj[13].f180k && !withoutT && disnum > 5) {
    // 時L年
    push180[3] = true
    push180[5] = true
  }
  if (obj[14].f180k && disnum > 5) {
    // 大L年
    push180[4] = true
    push180[5] = true
  }
  if (obj[15].f180k && disnum > 6) {
    // 年L月
    push180[0] = true
    push180[6] = true
  }
  if (obj[16].f180k && disnum > 6) {
    // 月L月
    push180[1] = true
    push180[6] = true
  }
  if (obj[17].f180k && disnum > 6) {
    // 日L月
    push180[2] = true
    push180[6] = true
  }
  if (obj[18].f180k && !withoutT && disnum > 6) {
    // 時L月
    push180[3] = true
    push180[6] = true
  }
  if (obj[19].f180k && disnum > 6) {
    // 大L月
    push180[4] = true
    push180[6] = true
  }
  if (obj[20].f180k && disnum > 6) {
    // L年月
    push180[5] = true
    push180[6] = true
  }
  if (obj[21].f180k && disnum > 7) {
    // 年L日
    push180[0] = true
    push180[7] = true
  }
  if (obj[22].f180k && disnum > 7) {
    // 月L日
    push180[1] = true
    push180[7] = true
  }
  if (obj[23].f180k && disnum > 7) {
    // 日L日
    push180[2] = true
    push180[7] = true
  }
  if (obj[24].f180k && !withoutT && disnum > 7) {
    // 時L日
    push180[3] = true
    push180[7] = true
  }
  if (obj[25].f180k && disnum > 7) {
    // 大L日
    push180[4] = true
    push180[7] = true
  }
  if (obj[26].f180k && disnum > 7) {
    // L年日
    push180[5] = true
    push180[7] = true
  }
  if (obj[27].f180k && disnum > 7) {
    // L月日
    push180[6] = true
    push180[7] = true
  }
  if (obj[28].f180k && disnum > 8) {
    // 年L時
    push180[0] = true
    push180[8] = true
  }
  if (obj[29].f180k && disnum > 8) {
    // 月L時
    push180[1] = true
    push180[8] = true
  }
  if (obj[30].f180k && disnum > 8) {
    // 日L時
    push180[2] = true
    push180[8] = true
  }
  if (obj[31].f180k && !withoutT && disnum > 8) {
    // 時L時
    push180[3] = true
    push180[8] = true
  }
  if (obj[32].f180k && disnum > 8) {
    // 大L時
    push180[4] = true
    push180[8] = true
  }
  if (obj[33].f180k && disnum > 8) {
    // L年時
    push180[5] = true
    push180[8] = true
  }
  if (obj[34].f180k && disnum > 8) {
    // L月時
    push180[6] = true
    push180[8] = true
  }
  if (obj[35].f180k && disnum > 8) {
    // L日時
    push180[7] = true
    push180[8] = true
  }

  if (obj[0].ftopbottom) {
    // 年月
    pushtopbottom[0] = true
    pushtopbottom[1] = true
  }
  if (obj[1].ftopbottom) {
    // 年日
    pushtopbottom[2] = true
    pushtopbottom[0] = true
  }
  if (obj[2].ftopbottom) {
    // 月日
    pushtopbottom[2] = true
    pushtopbottom[1] = true
  }
  if (obj[3].ftopbottom && !withoutT) {
    // 年時
    pushtopbottom[0] = true
    pushtopbottom[3] = true
  }
  if (obj[4].ftopbottom && !withoutT) {
    // 月時
    pushtopbottom[1] = true
    pushtopbottom[3] = true
  }
  if (obj[5].ftopbottom && !withoutT) {
    // 日時
    pushtopbottom[2] = true
    pushtopbottom[3] = true
  }
  if (obj[6].ftopbottom && disnum > 4) {
    // 年大
    pushtopbottom[0] = true
    pushtopbottom[4] = true
  }
  if (obj[7].ftopbottom && disnum > 4) {
    // 月大
    pushtopbottom[1] = true
    pushtopbottom[4] = true
  }
  if (obj[8].ftopbottom && disnum > 4) {
    // 日大
    pushtopbottom[2] = true
    pushtopbottom[4] = true
  }
  if (obj[9].ftopbottom && !withoutT && disnum > 4) {
    // 時大
    pushtopbottom[3] = true
    pushtopbottom[4] = true
  }
  if (obj[10].ftopbottom && disnum > 5) {
    // 年L年
    pushtopbottom[0] = true
    pushtopbottom[5] = true
  }
  if (obj[11].ftopbottom && disnum > 5) {
    // 月L年
    pushtopbottom[1] = true
    pushtopbottom[5] = true
  }
  if (obj[12].ftopbottom && disnum > 5) {
    // 日L年
    pushtopbottom[2] = true
    pushtopbottom[5] = true
  }
  if (obj[13].ftopbottom && !withoutT && disnum > 5) {
    // 時L年
    pushtopbottom[3] = true
    pushtopbottom[5] = true
  }
  if (obj[14].ftopbottom && disnum > 5) {
    // 大L年
    pushtopbottom[4] = true
    pushtopbottom[5] = true
  }
  if (obj[15].ftopbottom && disnum > 6) {
    // 年L月
    pushtopbottom[0] = true
    pushtopbottom[6] = true
  }
  if (obj[16].ftopbottom && disnum > 6) {
    // 月L月
    pushtopbottom[1] = true
    pushtopbottom[6] = true
  }
  if (obj[17].ftopbottom && disnum > 6) {
    // 日L月
    pushtopbottom[2] = true
    pushtopbottom[6] = true
  }
  if (obj[18].ftopbottom && !withoutT && disnum > 6) {
    // 時L月
    pushtopbottom[3] = true
    pushtopbottom[6] = true
  }
  if (obj[19].ftopbottom && disnum > 6) {
    // 大L月
    pushtopbottom[4] = true
    pushtopbottom[6] = true
  }
  if (obj[20].ftopbottom && disnum > 6) {
    // L年月
    pushtopbottom[5] = true
    pushtopbottom[6] = true
  }
  if (obj[21].ftopbottom && disnum > 7) {
    // 年L日
    pushtopbottom[0] = true
    pushtopbottom[7] = true
  }
  if (obj[22].ftopbottom && disnum > 7) {
    // 月L日
    pushtopbottom[1] = true
    pushtopbottom[7] = true
  }
  if (obj[23].ftopbottom && disnum > 7) {
    // 日L日
    pushtopbottom[2] = true
    pushtopbottom[7] = true
  }
  if (obj[24].ftopbottom && !withoutT && disnum > 7) {
    // 時L日
    pushtopbottom[3] = true
    pushtopbottom[7] = true
  }
  if (obj[25].ftopbottom && disnum > 7) {
    // 大L日
    pushtopbottom[4] = true
    pushtopbottom[7] = true
  }
  if (obj[26].ftopbottom && disnum > 7) {
    // L年日
    pushtopbottom[5] = true
    pushtopbottom[7] = true
  }
  if (obj[27].ftopbottom && disnum > 7) {
    // L月日
    pushtopbottom[6] = true
    pushtopbottom[7] = true
  }
  if (obj[28].ftopbottom && disnum > 8) {
    // 年L時
    pushtopbottom[0] = true
    pushtopbottom[8] = true
  }
  if (obj[29].ftopbottom && disnum > 8) {
    // 月L時
    pushtopbottom[1] = true
    pushtopbottom[8] = true
  }
  if (obj[30].ftopbottom && disnum > 8) {
    // 日L時
    pushtopbottom[2] = true
    pushtopbottom[8] = true
  }
  if (obj[31].ftopbottom && !withoutT && disnum > 8) {
    // 時L時
    pushtopbottom[3] = true
    pushtopbottom[8] = true
  }
  if (obj[32].ftopbottom && disnum > 8) {
    // 大L時
    pushtopbottom[4] = true
    pushtopbottom[8] = true
  }
  if (obj[33].ftopbottom && disnum > 8) {
    // L年時
    pushtopbottom[5] = true
    pushtopbottom[8] = true
  }
  if (obj[34].ftopbottom && disnum > 8) {
    // L月時
    pushtopbottom[6] = true
    pushtopbottom[8] = true
  }
  if (obj[35].ftopbottom && disnum > 8) {
    // L日時
    pushtopbottom[7] = true
    pushtopbottom[8] = true
  }

  if (obj[0].f90m) {
    // 年月
    push90[0] = true
    push90[1] = true
  }
  if (obj[1].f90m) {
    // 年日
    push90[2] = true
    push90[0] = true
  }
  if (obj[2].f90m) {
    // 月日
    push90[2] = true
    push90[1] = true
  }
  if (obj[3].f90m && !withoutT) {
    // 年時
    push90[0] = true
    push90[3] = true
  }
  if (obj[4].f90m && !withoutT) {
    // 月時
    push90[1] = true
    push90[3] = true
  }
  if (obj[5].f90m && !withoutT) {
    // 日時
    push90[2] = true
    push90[3] = true
  }
  if (obj[6].f90m && disnum > 4) {
    // 年大
    push90[0] = true
    push90[4] = true
  }
  if (obj[7].f90m && disnum > 4) {
    // 月大
    push90[1] = true
    push90[4] = true
  }
  if (obj[8].f90m && disnum > 4) {
    // 日大
    push90[2] = true
    push90[4] = true
  }
  if (obj[9].f90m && !withoutT && disnum > 4) {
    // 時大
    push90[3] = true
    push90[4] = true
  }
  if (obj[10].f90m && disnum > 5) {
    // 年L年
    push90[0] = true
    push90[5] = true
  }
  if (obj[11].f90m && disnum > 5) {
    // 月L年
    push90[1] = true
    push90[5] = true
  }
  if (obj[12].f90m && disnum > 5) {
    // 日L年
    push90[2] = true
    push90[5] = true
  }
  if (obj[13].f90m && !withoutT && disnum > 5) {
    // 時L年
    push90[3] = true
    push90[5] = true
  }
  if (obj[14].f90m && disnum > 5) {
    // 大L年
    push90[4] = true
    push90[5] = true
  }
  if (obj[15].f90m && disnum > 6) {
    // 年L月
    push90[0] = true
    push90[6] = true
  }
  if (obj[16].f90m && disnum > 6) {
    // 月L月
    push90[1] = true
    push90[6] = true
  }
  if (obj[17].f90m && disnum > 6) {
    // 日L月
    push90[2] = true
    push90[6] = true
  }
  if (obj[18].f90m && !withoutT && disnum > 6) {
    // 時L月
    push90[3] = true
    push90[6] = true
  }
  if (obj[19].f90m && disnum > 6) {
    // 大L月
    push90[4] = true
    push90[6] = true
  }
  if (obj[20].f90m && disnum > 6) {
    // L年月
    push90[5] = true
    push90[6] = true
  }
  if (obj[21].f90m && disnum > 7) {
    // 年L日
    push90[0] = true
    push90[7] = true
  }
  if (obj[22].f90m && disnum > 7) {
    // 月L日
    push90[1] = true
    push90[7] = true
  }
  if (obj[23].f90m && disnum > 7) {
    // 日L日
    push90[2] = true
    push90[7] = true
  }
  if (obj[24].f90m && !withoutT && disnum > 7) {
    // 時L日
    push90[3] = true
    push90[7] = true
  }
  if (obj[25].f90m && disnum > 7) {
    // 大L日
    push90[4] = true
    push90[7] = true
  }
  if (obj[26].f90m && disnum > 7) {
    // L年日
    push90[5] = true
    push90[7] = true
  }
  if (obj[27].f90m && disnum > 7) {
    // L月日
    push90[6] = true
    push90[7] = true
  }
  if (obj[28].f90m && disnum > 8) {
    // 年L時
    push90[0] = true
    push90[8] = true
  }
  if (obj[29].f90m && disnum > 8) {
    // 月L時
    push90[1] = true
    push90[8] = true
  }
  if (obj[30].f90m && disnum > 8) {
    // 日L時
    push90[2] = true
    push90[8] = true
  }
  if (obj[31].f90m && !withoutT && disnum > 8) {
    // 時L時
    push90[3] = true
    push90[8] = true
  }
  if (obj[32].f90m && disnum > 8) {
    // 大L時
    push90[4] = true
    push90[8] = true
  }
  if (obj[33].f90m && disnum > 8) {
    // L年時
    push90[5] = true
    push90[8] = true
  }
  if (obj[34].f90m && disnum > 8) {
    // L月時
    push90[6] = true
    push90[8] = true
  }
  if (obj[35].f90m && disnum > 8) {
    // L日時
    push90[7] = true
    push90[8] = true
  }

  if (obj[0].f150m) {
    // 年月
    push150[0] = true
    push150[1] = true
  }
  if (obj[1].f150m) {
    // 年日
    push150[2] = true
    push150[0] = true
  }
  if (obj[2].f150m) {
    // 月日
    push150[2] = true
    push150[1] = true
  }
  if (obj[3].f150m && !withoutT) {
    // 年時
    push150[0] = true
    push150[3] = true
  }
  if (obj[4].f150m && !withoutT) {
    // 月時
    push150[1] = true
    push150[3] = true
  }
  if (obj[5].f150m && !withoutT) {
    // 日時
    push150[2] = true
    push150[3] = true
  }
  if (obj[6].f150m && disnum > 4) {
    // 年大
    push150[0] = true
    push150[4] = true
  }
  if (obj[7].f150m && disnum > 4) {
    // 月大
    push150[1] = true
    push150[4] = true
  }
  if (obj[8].f150m && disnum > 4) {
    // 日大
    push150[2] = true
    push150[4] = true
  }
  if (obj[9].f150m && !withoutT && disnum > 4) {
    // 時大
    push150[3] = true
    push150[4] = true
  }
  if (obj[10].f150m && disnum > 5) {
    // 年L年
    push150[0] = true
    push150[5] = true
  }
  if (obj[11].f150m && disnum > 5) {
    // 月L年
    push150[1] = true
    push150[5] = true
  }
  if (obj[12].f150m && disnum > 5) {
    // 日L年
    push150[2] = true
    push150[5] = true
  }
  if (obj[13].f150m && !withoutT && disnum > 5) {
    // 時L年
    push150[3] = true
    push150[5] = true
  }
  if (obj[14].f150m && disnum > 5) {
    // 大L年
    push150[4] = true
    push150[5] = true
  }
  if (obj[15].f150m && disnum > 6) {
    // 年L月
    push150[0] = true
    push150[6] = true
  }
  if (obj[16].f150m && disnum > 6) {
    // 月L月
    push150[1] = true
    push150[6] = true
  }
  if (obj[17].f150m && disnum > 6) {
    // 日L月
    push150[2] = true
    push150[6] = true
  }
  if (obj[18].f150m && !withoutT && disnum > 6) {
    // 時L月
    push150[3] = true
    push150[6] = true
  }
  if (obj[19].f150m && disnum > 6) {
    // 大L月
    push150[4] = true
    push150[6] = true
  }
  if (obj[20].f150m && disnum > 6) {
    // L年月
    push150[5] = true
    push150[6] = true
  }
  if (obj[21].f150m && disnum > 7) {
    // 年L日
    push150[0] = true
    push150[7] = true
  }
  if (obj[22].f150m && disnum > 7) {
    // 月L日
    push150[1] = true
    push150[7] = true
  }
  if (obj[23].f150m && disnum > 7) {
    // 日L日
    push150[2] = true
    push150[7] = true
  }
  if (obj[24].f150m && !withoutT && disnum > 7) {
    // 時L日
    push150[3] = true
    push150[7] = true
  }
  if (obj[25].f150m && disnum > 7) {
    // 大L日
    push150[4] = true
    push150[7] = true
  }
  if (obj[26].f150m && disnum > 7) {
    // L年日
    push150[5] = true
    push150[7] = true
  }
  if (obj[27].f150m && disnum > 7) {
    // L月日
    push150[6] = true
    push150[7] = true
  }
  if (obj[28].f150m && disnum > 8) {
    // 年L時
    push150[0] = true
    push150[8] = true
  }
  if (obj[29].f150m && disnum > 8) {
    // 月L時
    push150[1] = true
    push150[8] = true
  }
  if (obj[30].f150m && disnum > 8) {
    // 日L時
    push150[2] = true
    push150[8] = true
  }
  if (obj[31].f150m && !withoutT && disnum > 8) {
    // 時L時
    push150[3] = true
    push150[8] = true
  }
  if (obj[32].f150m && disnum > 8) {
    // 大L時
    push150[4] = true
    push150[8] = true
  }
  if (obj[33].f150m && disnum > 8) {
    // L年時
    push150[5] = true
    push150[8] = true
  }
  if (obj[34].f150m && disnum > 8) {
    // L月時
    push150[6] = true
    push150[8] = true
  }
  if (obj[35].f150m && disnum > 8) {
    // L日時
    push150[7] = true
    push150[8] = true
  }

  if (obj[0].f60m) {
    // 年月
    push60[0] = true
    push60[1] = true
  }
  if (obj[1].f60m) {
    // 年日
    push60[2] = true
    push60[0] = true
  }
  if (obj[2].f60m) {
    // 月日
    push60[2] = true
    push60[1] = true
  }
  if (obj[3].f60m && !withoutT) {
    // 年時
    push60[0] = true
    push60[3] = true
  }
  if (obj[4].f60m && !withoutT) {
    // 月時
    push60[1] = true
    push60[3] = true
  }
  if (obj[5].f60m && !withoutT) {
    // 日時
    push60[2] = true
    push60[3] = true
  }
  if (obj[6].f60m && disnum > 4) {
    // 年大
    push60[0] = true
    push60[4] = true
  }
  if (obj[7].f60m && disnum > 4) {
    // 月大
    push60[1] = true
    push60[4] = true
  }
  if (obj[8].f60m && disnum > 4) {
    // 日大
    push60[2] = true
    push60[4] = true
  }
  if (obj[9].f60m && !withoutT && disnum > 4) {
    // 時大
    push60[3] = true
    push60[4] = true
  }
  if (obj[10].f60m && disnum > 5) {
    // 年L年
    push60[0] = true
    push60[5] = true
  }
  if (obj[11].f60m && disnum > 5) {
    // 月L年
    push60[1] = true
    push60[5] = true
  }
  if (obj[12].f60m && disnum > 5) {
    // 日L年
    push60[2] = true
    push60[5] = true
  }
  if (obj[13].f60m && !withoutT && disnum > 5) {
    // 時L年
    push60[3] = true
    push60[5] = true
  }
  if (obj[14].f60m && disnum > 5) {
    // 大L年
    push60[4] = true
    push60[5] = true
  }
  if (obj[15].f60m && disnum > 6) {
    // 年L月
    push60[0] = true
    push60[6] = true
  }
  if (obj[16].f60m && disnum > 6) {
    // 月L月
    push60[1] = true
    push60[6] = true
  }
  if (obj[17].f60m && disnum > 6) {
    // 日L月
    push60[2] = true
    push60[6] = true
  }
  if (obj[18].f60m && !withoutT && disnum > 6) {
    // 時L月
    push60[3] = true
    push60[6] = true
  }
  if (obj[19].f60m && disnum > 6) {
    // 大L月
    push60[4] = true
    push60[6] = true
  }
  if (obj[20].f60m && disnum > 6) {
    // L年月
    push60[5] = true
    push60[6] = true
  }
  if (obj[21].f60m && disnum > 7) {
    // 年L日
    push60[0] = true
    push60[7] = true
  }
  if (obj[22].f60m && disnum > 7) {
    // 月L日
    push60[1] = true
    push60[7] = true
  }
  if (obj[23].f60m && disnum > 7) {
    // 日L日
    push60[2] = true
    push60[7] = true
  }
  if (obj[24].f60m && !withoutT && disnum > 7) {
    // 時L日
    push60[3] = true
    push60[7] = true
  }
  if (obj[25].f60m && disnum > 7) {
    // 大L日
    push60[4] = true
    push60[7] = true
  }
  if (obj[26].f60m && disnum > 7) {
    // L年日
    push60[5] = true
    push60[7] = true
  }
  if (obj[27].f60m && disnum > 7) {
    // L月日
    push60[6] = true
    push60[7] = true
  }
  if (obj[28].f60m && disnum > 8) {
    // 年L時
    push60[0] = true
    push60[8] = true
  }
  if (obj[29].f60m && disnum > 8) {
    // 月L時
    push60[1] = true
    push60[8] = true
  }
  if (obj[30].f60m && disnum > 8) {
    // 日L時
    push60[2] = true
    push60[8] = true
  }
  if (obj[31].f60m && !withoutT && disnum > 8) {
    // 時L時
    push60[3] = true
    push60[8] = true
  }
  if (obj[32].f60m && disnum > 8) {
    // 大L時
    push60[4] = true
    push60[8] = true
  }
  if (obj[33].f60m && disnum > 8) {
    // L年時
    push60[5] = true
    push60[8] = true
  }
  if (obj[34].f60m && disnum > 8) {
    // L月時
    push60[6] = true
    push60[8] = true
  }
  if (obj[35].f60m && disnum > 8) {
    // L日時
    push60[7] = true
    push60[8] = true
  }

  return [asf, mergeIn3, merginIn3Grp, mergeIn6, push180, pushtopbottom, push90, push150, push60]
}

function returnSixtyList(flag) {
  const lst = []
  const t = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  const b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
  let tp = 0
  let bp = 0
  if (flag) {
    for (let i = 0; i < 60; i += 1) {
      lst.push([t[tp], b[bp]])
      if (tp === 9) {
        tp = 0
      } else {
        tp += 1
      }
      if (bp === 11) {
        bp = 0
      } else {
        bp += 1
      }
    }
  } else {
    for (let i = 0; i < 60; i += 1) {
      lst.push([t[tp], b[bp]])
      if (tp === 0) {
        tp = 9
      } else {
        tp -= 1
      }
      if (bp === 0) {
        bp = 11
      } else {
        bp -= 1
      }
    }
  }
  return lst
}

export function getSelYearLst(year) {
  const a = getYearrTop(year)
  const b = getYearBottom(year)
  const lst = returnSixtyList(true)
  let cp = 0
  for (let i = 0; i < lst.length; i += 1) {
    if (a === lst[i][0] && b === lst[i][1]) {
      cp = i
    }
  }
  const resLst = []
  for (let i = 0; i < 80; i += 1) {
    resLst.push(lst[cp])
    if (cp === 59) {
      cp = 0
    } else {
      cp += 1
    }
  }
  return resLst
}

export function getsmalldestiny(sex, yrtop, timetop, timebottom) {
  let sortfor = true
  if (sex && yrtop % 2 === 0) {
    sortfor = false
  }
  if (sex && yrtop % 2 !== 0) {
    sortfor = true
  }
  if (!sex && yrtop % 2 !== 0) {
    sortfor = false
  }
  if (!sex && yrtop % 2 === 0) {
    sortfor = true
  }
  // console.log(sex, yrtop, timetop, timebottom, sortfor)
  // }
  const lst = returnSixtyList(sortfor)
  let cp = 0
  for (let i = 0; i < lst.length; i += 1) {
    if (timetop === lst[i][0] && timebottom === lst[i][1]) {
      cp = i
    }
  }
  const resLst = []
  for (let i = 0; i < 80; i += 1) {
    if (cp === 59) {
      cp = 0
    } else {
      cp += 1
    }
    resLst.push(lst[cp])
  }
  return resLst
}

export function getYr24Event(yr) {
  const b = get24EventDateList(getUserUseSpringDay(yr, 1, 2, 1, 1))
  const a = get24EventDateList(getUserUseSpringDay(yr, 3, 2, 1, 1))
  const c = [
    b[22], b[23], a[0], a[1],
    a[2], a[3], a[4], a[5],
    a[6], a[7], a[8], a[9],
    a[10], a[11], a[12], a[13],
    a[14], a[15], a[16], a[17],
    a[18], a[19], a[20], a[21],
  ]
  return c
}

export function calcWordValue(num1, num2) {
  // 生系數
  // const urate = 1.5
  const uurate = 1.0
  const lurate = 0.8
  // 剋系數
  // const urate = 1.5
  // const uurate = 0.5
  // const lurate = 0.8
  const a = num1[0]
  const b = num2
  for (let i = 0; i < b.length; i += 1) {
    if ((a.num === 1 || a.num === 2) && (b[i].num === 9 || b[i].num === 10)) {
      a.value += b[i].value
      b[i].value *= lurate
    }
    if ((a.num === 1 || a.num === 2) && (b[i].num === 1 || b[i].num === 2)) {
      a.value = (a.value + b[i].value) * uurate
      b[i].value *= uurate
      // b[i].value *= lurate
    }
    if ((a.num === 1 || a.num === 2) && (b[i].num === 3 || b[i].num === 4)) {
      a.value -= (b[i].value / 2)
      b[i].value *= uurate
    }
    if ((a.num === 1 || a.num === 2) && (b[i].num === 5 || b[i].num === 6 || b[i].num === 7 || b[i].num === 8)) {
      a.value -= (b[i].value / 2)
      b[i].value *= lurate
    }
    a.value = Math.floor(a.value)
    b[i].value = Math.floor(b[i].value)
  }

  for (let i = 0; i < b.length; i += 1) {
    if ((a.num === 3 || a.num === 4) && (b[i].num === 1 || b[i].num === 2)) {
      a.value += b[i].value
      b[i].value *= lurate
    }
    if ((a.num === 3 || a.num === 4) && (b[i].num === 3 || b[i].num === 4)) {
      a.value = (a.value + b[i].value) * uurate
      b[i].value *= uurate
      // b[i].value *= lurate
    }
    if ((a.num === 3 || a.num === 4) && (b[i].num === 5 || b[i].num === 6)) {
      a.value -= (b[i].value / 2)
      b[i].value *= uurate
    }
    if ((a.num === 3 || a.num === 4) && (b[i].num === 7 || b[i].num === 8 || b[i].num === 9 || b[i].num === 10)) {
      a.value -= (b[i].value / 2)
      b[i].value *= lurate
    }
    a.value = Math.floor(a.value)
    b[i].value = Math.floor(b[i].value)
  }

  for (let i = 0; i < b.length; i += 1) {
    if ((a.num === 5 || a.num === 6) && (b[i].num === 3 || b[i].num === 4)) {
      a.value += b[i].value
      b[i].value *= lurate
    }
    if ((a.num === 5 || a.num === 6) && (b[i].num === 5 || b[i].num === 6)) {
      a.value = (a.value + b[i].value) * uurate
      b[i].value *= uurate
      // b[i].value *= lurate
    }
    if ((a.num === 5 || a.num === 6) && (b[i].num === 7 || b[i].num === 8)) {
      a.value -= (b[i].value / 2)
      b[i].value *= uurate
    }
    if ((a.num === 5 || a.num === 6) && (b[i].num === 9 || b[i].num === 10 || b[i].num === 1 || b[i].num === 2)) {
      a.value -= (b[i].value / 2)
      b[i].value *= lurate
    }
    a.value = Math.floor(a.value)
    b[i].value = Math.floor(b[i].value)
  }

  for (let i = 0; i < b.length; i += 1) {
    if ((a.num === 7 || a.num === 8) && (b[i].num === 5 || b[i].num === 6)) {
      a.value += b[i].value
      b[i].value *= lurate
    }
    if ((a.num === 7 || a.num === 8) && (b[i].num === 7 || b[i].num === 8)) {
      a.value = (a.value + b[i].value) * uurate
      b[i].value *= uurate
      // b[i].value *= lurate
    }
    if ((a.num === 7 || a.num === 8) && (b[i].num === 9 || b[i].num === 10)) {
      a.value -= (b[i].value / 2)
      b[i].value *= uurate
    }
    if ((a.num === 7 || a.num === 8) && (b[i].num === 1 || b[i].num === 2 || b[i].num === 3 || b[i].num === 4)) {
      a.value -= (b[i].value / 2)
      b[i].value *= lurate
    }
    a.value = Math.floor(a.value)
    b[i].value = Math.floor(b[i].value)
  }

  for (let i = 0; i < b.length; i += 1) {
    if ((a.num === 9 || a.num === 10) && (b[i].num === 7 || b[i].num === 8)) {
      a.value += b[i].value
      b[i].value *= lurate
    }
    if ((a.num === 9 || a.num === 10) && (b[i].num === 9 || b[i].num === 10)) {
      a.value = (a.value + b[i].value) * uurate
      b[i].value *= uurate
      // b[i].value *= lurate
    }
    if ((a.num === 9 || a.num === 10) && (b[i].num === 1 || b[i].num === 2)) {
      a.value -= (b[i].value / 2)
      b[i].value *= uurate
    }
    if ((a.num === 9 || a.num === 10) && (b[i].num === 3 || b[i].num === 4 || b[i].num === 5 || b[i].num === 6)) {
      a.value -= (b[i].value / 2)
      b[i].value *= lurate
    }
    a.value = Math.floor(a.value)
    b[i].value = Math.floor(b[i].value)
  }

  return [num1, num2]
}

export function calcOneTownVal(num1, num2) {
  return calcWordValue(getWordPowerT(num1), getWordPowerB(num2))
}

export function getUserTown(yr, mon, day, hr, sex, withoutT) {
  const tm = gettoptowncalc(false, 4, yr, mon, day,
    hr, 0, false, sex,
    yr, mon, day, hr, 0)
  const bm = getbottomtowncalc(false, 4, yr, mon, day,
    hr, 0, false, sex,
    yr, mon, day, hr, 0)

  let merge = []
  if (withoutT === 0) {
    merge = [
      calcOneTownVal(tm[0][0][0], bm[0][0][1]),
      calcOneTownVal(tm[0][1][0], bm[0][1][1]),
      calcOneTownVal(tm[0][2][0], bm[0][2][1]),
      [[{ num: '0', value: 0 }], [{ num: '0', value: 0 }]],
      calcOneTownVal(tm[0][4][0], bm[0][4][1]),
      calcOneTownVal(tm[0][5][0], bm[0][5][1]),
      calcOneTownVal(tm[0][6][0], bm[0][6][1]),
      calcOneTownVal(tm[0][7][0], bm[0][7][1]),
      calcOneTownVal(tm[0][8][0], bm[0][8][1]),
    ]
  } else {
    merge = [
      calcOneTownVal(tm[0][0][0], bm[0][0][1]),
      calcOneTownVal(tm[0][1][0], bm[0][1][1]),
      calcOneTownVal(tm[0][2][0], bm[0][2][1]),
      calcOneTownVal(tm[0][3][0], bm[0][3][1]),
      calcOneTownVal(tm[0][4][0], bm[0][4][1]),
      calcOneTownVal(tm[0][5][0], bm[0][5][1]),
      calcOneTownVal(tm[0][6][0], bm[0][6][1]),
      calcOneTownVal(tm[0][7][0], bm[0][7][1]),
      calcOneTownVal(tm[0][8][0], bm[0][8][1]),
    ]
  }

  const lastLst = []
  const calcLst = []
  for (let i = 0; i < 4; i += 1) {
    lastLst.push(merge[i])
  }

  // single calc
  for (let i = 0; i < lastLst.length; i += 1) {
    for (let j = 0; j < lastLst[i].length; j += 1) {
      for (let k = 0; k < lastLst[i][j].length; k += 1) {
        if (j === 0) {
          const aa = lastLst[i][j][k]
          aa.value *= 2.4
          calcLst.push(aa)
        } else {
          calcLst.push(lastLst[i][j][k])
        }
      }
    }
  }

  const grpLsttmp = _.groupBy(calcLst, 'num')
  const grpLstObj = {
    1: grpLsttmp[1] ? grpLsttmp[1] : [],
    2: grpLsttmp[2] ? grpLsttmp[2] : [],
    3: grpLsttmp[3] ? grpLsttmp[3] : [],
    4: grpLsttmp[4] ? grpLsttmp[4] : [],
    5: grpLsttmp[5] ? grpLsttmp[5] : [],
    6: grpLsttmp[6] ? grpLsttmp[6] : [],
    7: grpLsttmp[7] ? grpLsttmp[7] : [],
    8: grpLsttmp[8] ? grpLsttmp[8] : [],
    9: grpLsttmp[9] ? grpLsttmp[9] : [],
    10: grpLsttmp[10] ? grpLsttmp[10] : [],
  }

  const grpLst = [
    _.sumBy(grpLstObj[1], 'value'),
    _.sumBy(grpLstObj[2], 'value'),
    _.sumBy(grpLstObj[3], 'value'),
    _.sumBy(grpLstObj[4], 'value'),
    _.sumBy(grpLstObj[5], 'value'),
    _.sumBy(grpLstObj[6], 'value'),
    _.sumBy(grpLstObj[7], 'value'),
    _.sumBy(grpLstObj[8], 'value'),
    _.sumBy(grpLstObj[9], 'value'),
    _.sumBy(grpLstObj[10], 'value'),
  ]
  return grpLst
}

export function getUserTownCustom(yr, mon, day, hr, sex, withoutT, syr, smon, sday, shr, calcnum) {
  const tm = gettoptowncalc(withoutT, calcnum, syr, smon, sday,
    shr, 0, false, sex,
    yr, mon, day, hr, 0)
  const bm = getbottomtowncalc(withoutT, calcnum, syr, smon, sday,
    shr, 0, false, sex,
    yr, mon, day, hr, 0)

  let merge = []
  if (withoutT === 0) {
    merge = [
      calcOneTownVal(tm[0][0][0], bm[0][0][1]),
      calcOneTownVal(tm[0][1][0], bm[0][1][1]),
      calcOneTownVal(tm[0][2][0], bm[0][2][1]),
      [[{ num: '0', value: 0 }], [{ num: '0', value: 0 }]],
      calcOneTownVal(tm[0][4][0], bm[0][4][1]),
      calcOneTownVal(tm[0][5][0], bm[0][5][1]),
      calcOneTownVal(tm[0][6][0], bm[0][6][1]),
      calcOneTownVal(tm[0][7][0], bm[0][7][1]),
      calcOneTownVal(tm[0][8][0], bm[0][8][1]),
    ]
  } else {
    merge = [
      calcOneTownVal(tm[0][0][0], bm[0][0][1]),
      calcOneTownVal(tm[0][1][0], bm[0][1][1]),
      calcOneTownVal(tm[0][2][0], bm[0][2][1]),
      calcOneTownVal(tm[0][3][0], bm[0][3][1]),
      calcOneTownVal(tm[0][4][0], bm[0][4][1]),
      calcOneTownVal(tm[0][5][0], bm[0][5][1]),
      calcOneTownVal(tm[0][6][0], bm[0][6][1]),
      calcOneTownVal(tm[0][7][0], bm[0][7][1]),
      calcOneTownVal(tm[0][8][0], bm[0][8][1]),
    ]
  }

  const lastLst = []
  const calcLst = []
  for (let i = 0; i < calcnum; i += 1) {
    lastLst.push(merge[i])
  }

  // single calc
  for (let i = 0; i < lastLst.length; i += 1) {
    for (let j = 0; j < lastLst[i].length; j += 1) {
      for (let k = 0; k < lastLst[i][j].length; k += 1) {
        if (j === 0) {
          const aa = lastLst[i][j][k]
          aa.value *= 2.4
          calcLst.push(aa)
        } else {
          calcLst.push(lastLst[i][j][k])
        }
      }
    }
  }

  const grpLsttmp = _.groupBy(calcLst, 'num')
  const grpLstObj = {
    1: grpLsttmp[1] ? grpLsttmp[1] : [],
    2: grpLsttmp[2] ? grpLsttmp[2] : [],
    3: grpLsttmp[3] ? grpLsttmp[3] : [],
    4: grpLsttmp[4] ? grpLsttmp[4] : [],
    5: grpLsttmp[5] ? grpLsttmp[5] : [],
    6: grpLsttmp[6] ? grpLsttmp[6] : [],
    7: grpLsttmp[7] ? grpLsttmp[7] : [],
    8: grpLsttmp[8] ? grpLsttmp[8] : [],
    9: grpLsttmp[9] ? grpLsttmp[9] : [],
    10: grpLsttmp[10] ? grpLsttmp[10] : [],
  }

  const grpLst = [
    _.sumBy(grpLstObj[1], 'value'),
    _.sumBy(grpLstObj[2], 'value'),
    _.sumBy(grpLstObj[3], 'value'),
    _.sumBy(grpLstObj[4], 'value'),
    _.sumBy(grpLstObj[5], 'value'),
    _.sumBy(grpLstObj[6], 'value'),
    _.sumBy(grpLstObj[7], 'value'),
    _.sumBy(grpLstObj[8], 'value'),
    _.sumBy(grpLstObj[9], 'value'),
    _.sumBy(grpLstObj[10], 'value'),
  ]

  return grpLst
}
